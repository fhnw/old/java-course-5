package ch.kri.fhbb.BeanLabel;

import java.awt.*;
import java.awt.font.LineMetrics;
import java.util.*;

public class BeanLabel extends Canvas {

	private static final long serialVersionUID = 333098844414572027L;
	// Einstellungen
	protected String label; // Inhalt
	protected int margin_width; // Abstand links und rechts
	protected int margin_height; // Abstand oben und unten

	// Sonstiges
	protected int num_lines; // Anzahl ben�tigter Zeilen
	protected String[] lines; // Array mit den Zeilen
	protected int[] line_widths; // Breite der Zeilen
	int max_width; // maximale Breite
	int line_height; // H�he des Fonts
	int line_ascent; // H�he des Fonts uber dem Baseline
	protected boolean measured = false; // Attribute schon berechnet?

	// Normaler Constructor
	public BeanLabel(String label, int margin_width, int margin_height) {
		this.label = label;
		this.margin_width = margin_width;
		this.margin_height = margin_height;
		newLines(); // aufsplitten
	}

	// Ein Bean muss einen Constructur ohne Parameter anbieten
	public BeanLabel() {
		this("Your Label Here\nLine 2", 10, 10);
	}

	// Methoden, um die Attribute zu lesen bzw. zu setzen
	public void setLabel(String label) {
		this.label = label;
		newLines(); // aufsplitten
		measured = false;
		repaint();
	}

	public String getLabel() {
		return label;
	}

	public void setFont(Font f) { // Unterschiedlich von Superclass
		super.setFont(f);
		measured = false;
		repaint();
	}

	// public Font getFont() // von Superclass

	public void setMarginWidth(int margin_width) {
		this.margin_width = margin_width;
		repaint();
	}

	public int getMarginWidth() {
		return margin_width;
	}

	public void setMarginHeight(int margin_height) {
		this.margin_height = margin_height;
		repaint();
	}

	public int getMarginHeight() {
		return margin_height;
	}

	public void changeBackground() {
		Color c;
		c = this.getBackground();
		if (c.getRed() > 150)
			c = new Color(0, 255, 0);
		else if (c.getGreen() > 150)
			c = new Color(0, 0, 255);
		else
			c = new Color(255, 0, 0);
		this.setBackground(c);
	}

	// Ein LayoutManager fragt nach unserer gew�nschten Gr�sse
	public Dimension preferredSize() {
		if (!measured)
			measure();
		int width = max_width + 2 * margin_width;
		int height = num_lines * line_height + 2 * margin_height;
		return new Dimension(width, height);
	}

	// ein LayoutManager fragt nach unserer minimalen Gr�sse.
	public Dimension minimumSize() {
		return preferredSize();
	}

	// Die Paint-Methode
	public void paint(Graphics g) {
		int x, y;
		Dimension size = this.getSize();
		if (!measured)
			measure();
		y = line_ascent + (size.height - num_lines * line_height) / 2;
		for (int i = 0; i < num_lines; i++, y += line_height) {
			x = (size.width - line_widths[i]) / 2;
			g.drawString(lines[i], x, y);
		}
	}

	// in Zeilen aufsplitten
	protected synchronized void newLines() {
		StringTokenizer t = new StringTokenizer(label, "\n");
		num_lines = t.countTokens();
		lines = new String[num_lines];
		line_widths = new int[num_lines];
		for (int i = 0; i < num_lines; i++)
			lines[i] = t.nextToken();
	}

	// Attribute berechnen
	protected synchronized void measure() {
		FontMetrics fm = this.getToolkit().getFontMetrics(this.getFont());
		line_height = fm.getHeight();
		line_ascent = fm.getAscent();
		max_width = 0;
		for (int i = 0; i < num_lines; i++) {
			line_widths[i] = fm.stringWidth(lines[i]);
			if (line_widths[i] > max_width)
				max_width = line_widths[i];
		}
		measured = true;
	}
}
