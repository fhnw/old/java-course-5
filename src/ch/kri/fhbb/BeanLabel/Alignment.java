package ch.kri.fhbb.BeanLabel;

// Die folgende Klasse definiert drei Konstanten. In anderen Sprachen
// nennt man solche Definitionen "Enumerated Types". Es geht hier um
// den Namen - es gibt keinen Inhalt.
public class Alignment {
    // Drei Instanzen vorbereiten
    public static final Alignment LEFT = new Alignment();
    public static final Alignment CENTER = new Alignment();
    public static final Alignment RIGHT = new Alignment();
    
    // Constructor privat, damit keine weitere Instanzen erzeugt werden kann
    private Alignment() {};
}