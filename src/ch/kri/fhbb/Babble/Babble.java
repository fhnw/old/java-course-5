package ch.kri.fhbb.Babble;

import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.util.*;

public class Babble extends javax.swing.JFrame {
	private static final long serialVersionUID = 3240162162033252771L;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JTextField txtFileIn;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JTextField txtFileOut;
	private javax.swing.JButton btnBabble;
	private javax.swing.JButton btnClose;
	private Hashtable<String, Integer> analysis = new Hashtable<String, Integer>();
	private int total = 0;

	/** Creates new form JFrame */
	public Babble() {
		jLabel1 = new javax.swing.JLabel();
		txtFileIn = new javax.swing.JTextField();
		jLabel2 = new javax.swing.JLabel();
		txtFileOut = new javax.swing.JTextField();
		btnBabble = new javax.swing.JButton();
		btnClose = new javax.swing.JButton();

		getContentPane().setLayout(new java.awt.GridLayout(3, 2));

		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				exitForm(evt);
			}
		});

		jLabel1.setText("Input Datei");
		getContentPane().add(jLabel1);

		getContentPane().add(txtFileIn);

		jLabel2.setText("Output Datei");
		getContentPane().add(jLabel2);

		getContentPane().add(txtFileOut);

		btnBabble.setText("Babble");
		btnBabble.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnBabbleMouseClicked(evt);
			}
		});

		getContentPane().add(btnBabble);

		btnClose.setText("Schliessen");
		btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnCloseMouseClicked(evt);
			}
		});

		getContentPane().add(btnClose);

		pack();
	}

	private void btnBabbleMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_btnBabbleMouseClicked
		ShowDialog errDialog;

		String fileNameIn = txtFileIn.getText();
		File fileIn = new File(fileNameIn);

		// Die Eingabe-Datei prufen
		if (!fileIn.exists()) { // existiert nicht
			errDialog = new ShowDialog("Fehler", "'" + fileNameIn
					+ "' existiert nicht!");
			errDialog.setVisible(true);
			return;
		}
		if (!fileIn.isFile()) { // ist ein Verzeichnis
			errDialog = new ShowDialog("Fehler", "'" + fileNameIn
					+ "' ist keine Datei!");
			errDialog.setVisible(true);
			return;
		}
		if (!fileIn.canRead()) { // Keine Lese-Rechte
			errDialog = new ShowDialog("Fehler", "'" + fileNameIn
					+ "' kann nicht gelesen werden!");
			errDialog.setVisible(true);
			return;
		}

		// alles scheint in Ordnung zu sein!
		BufferedReader streamIn = null;
		try {
			// Unsere Streams und Puffer erzeugen
			streamIn = new BufferedReader(new FileReader(fileIn));

			// In einer Schleife: Zeichen lesen, Analyse updaten - bis Fehler (EOF)
			String key;
			int value;
			Integer valueObject;
			char c3;
			char c1 = (char) streamIn.read();
			char c2 = (char) streamIn.read();
			int intChar = streamIn.read();
			while (intChar != -1) {
				c3 = (char) intChar;
				key = String.valueOf(c1) + String.valueOf(c2) + String.valueOf(c3);
				if (analysis.containsKey(key)) {
					valueObject = analysis.get(key);
					value = valueObject.intValue();
					analysis.put(key, new Integer(value + 1));
				} else { // new
					analysis.put(key, new Integer(1));
				}
				total++;

				c1 = c2;
				c2 = c3;
				intChar = streamIn.read();
			}
		} catch (Exception e) {
		} finally { // Ob Fehler oder nicht, wir mussen das Stream schliessen
			if (streamIn != null)
				try {
					streamIn.close();
				} catch (IOException e2) {
				}
		}

		// Die Analyse ist fertig
		ShowDialog zusammenfassung = new ShowDialog("Fertig", Integer
				.toString(total)
				+ " Keys gesehen, davon " + analysis.size() + " unterschiedlich");
		zusammenfassung.show();
	}

	private void btnCloseMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_btnCloseMouseClicked
		System.exit(0);
	}

	/** Exit the Application */
	private void exitForm(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_exitForm
		System.exit(0);
	}

	/**
	 * @param args
	 *          the command line arguments
	 */
	public static void main(String args[]) {
		new Babble().setVisible(true);
	}

	class ShowDialog extends javax.swing.JFrame {
		private static final long serialVersionUID = -602082173862769269L;

		private JFrame errorDialog;

		public ShowDialog(String title, String message) {
			errorDialog = new JFrame(title);
			Container frameContent = errorDialog.getContentPane();
			frameContent.setLayout(new GridLayout(1, 1));
			JLabel lblError = new JLabel("");
			frameContent.add(lblError);
			lblError.setText(message);
			errorDialog.pack();
		}

		public void show() {
			errorDialog.setLocation(100, 100);
			errorDialog.setVisible(true);
		}
	}

}
