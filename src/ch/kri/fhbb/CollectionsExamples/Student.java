package ch.kri.fhbb.CollectionsExamples;

public class Student {
  public String name;
  public String klasse;

  public Student(String name, String klasse) {
    this.name = name;
    this.klasse = klasse;
  }

  public String toString() {
    String out;
    out = "Name = " + this.name + ", Klasse = " + this.klasse;
    return out;
  }
}
