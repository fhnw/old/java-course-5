package ch.kri.fhbb.CollectionsExamples;

import java.util.*;

public class ArrayListMain {

	public static void main(String[] args) {
    ArrayList<Student> meine_liste = new ArrayList<Student>();
    Student tmpStudent;

    // Students erzeugen - in einem echten Programm wären
    // die Studenten vielleicht von einer Datenbank gelesen
    tmpStudent = new Student("Ralf Schmid", "3a");
    meine_liste.add(tmpStudent);
    
    tmpStudent = new Student("Sarah Múller", "3b");
    meine_liste.add(tmpStudent);

    tmpStudent = new Student("Hans Krissler", "2b");
    meine_liste.add(tmpStudent);

    // Alle Elemente anzeigen
    for (Iterator<Student> i = meine_liste.iterator(); i.hasNext(); ) {
    	tmpStudent = i.next();
    	System.out.println(tmpStudent.toString());
    }
	}    
}
