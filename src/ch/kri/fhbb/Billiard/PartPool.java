package ch.kri.fhbb.Billiard;

import java.awt.*;
import java.util.*;

public class PartPool extends ParentApplet implements Runnable {

	private static final long serialVersionUID = -7829737077634217657L;
	protected static int width = 400;
	protected static int height = 400;
	int r = 10;
	int dx = 11, dy = 7;
	Thread partpool = null;
	Kugel kugel = null;
	public static Random random = new Random();
	static Vector<Kugel> kugel_vector = new Vector<Kugel>();

	public void paint(Graphics g) {
		g.drawRect(0, 0, getSize().width - 1, getSize().height - 1);
		g.drawRect(1, 1, getSize().width - 2, getSize().height - 2);
		int xtmpstart = Integer.parseInt(getParameter("xstart"));
		int xtmpstop = Integer.parseInt(getParameter("xstop"));
		int ytmpstart = Integer.parseInt(getParameter("ystart"));
		int ytmpstop = Integer.parseInt(getParameter("ystop"));
		int x = 0;
		int y = 0;
		Color c;
		for (int i = 0; i < kugel_vector.size(); i++) {
			x = kugel_vector.elementAt(i).getX();
			y = kugel_vector.elementAt(i).getY();
			c = kugel_vector.elementAt(i).getColor();
			if ((x > xtmpstart) && (x < xtmpstop) && (y > ytmpstart)
					&& (y < ytmpstop)) {
				g.setColor(c);
				g.fillOval(x - r - xtmpstart, y - r - ytmpstart, r * 2, r * 2);
			}
		}
	}

	public void animate() {
		int x = kugel.getX();
		int y = kugel.getY();
		// Refelektiere Kugel falls Kante ohne Nachbarfeld
		if ((x - r + dx < 0) || (x + r + dx > PartPool.width))
			dx = -dx;
		if ((y - r + dy < 0) || (y + r + dy > PartPool.height))
			dy = -dy;
		// Bewege Kugel
		x += dx;
		y += dy;
		kugel.setX(x);
		kugel.setY(y);
		int xtmpstart = Integer.parseInt(getParameter("xstart"));
		int xtmpstop = Integer.parseInt(getParameter("xstop"));
		int ytmpstart = Integer.parseInt(getParameter("ystart"));
		int ytmpstop = Integer.parseInt(getParameter("ystop"));
		for (int i = 0; i < kugel_vector.size(); i++) {
			x = kugel_vector.elementAt(i).getX();
			y = kugel_vector.elementAt(i).getY();
			if ((x > xtmpstart - r - 1) && (x < xtmpstop + r + 1)
					&& (y > ytmpstart - r - 1) && (y < ytmpstop + r + 1))
				repaint();
		}
	}

	public void init() {
		addPartPool(this);
		int x = 0;
		int y = 0;
		if (kugel == null) {
			x = (int) (random.nextFloat() * width);
			if (x + r > width)
				x = width - r;
			y = (int) (random.nextFloat() * height);
			if (y + r > height)
				y = height - r;
			kugel = new Kugel(this, x, y, getParameter("color"));
			kugel_vector.addElement(kugel);
		}
	}

	public void start() {
		if (partpool == null) {
			partpool = new Thread(this);
			partpool.start();
		}
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(100);
				animate();
			} catch (InterruptedException e) {
			}
		}
	}

	public void stop() {
		if (partpool != null) {
			partpool.stop();
			partpool = null;
		}
	}

}

class Kugel {
	private int x;
	private int y;
	private Color color = Color.gray;
	private PartPool applet;

	public Kugel(PartPool ap, int x, int y, String color) {
		this.applet = ap;
		this.x = x;
		this.y = y;
		if (color.compareTo("red") == 0)
			this.color = Color.red;
		if (color.compareTo("blue") == 0)
			this.color = Color.blue;
		if (color.compareTo("yellow") == 0)
			this.color = Color.yellow;
		if (color.compareTo("green") == 0)
			this.color = Color.green;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public Color getColor() {
		return this.color;
	}

	public PartPool getApplet() {
		return this.applet;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
}
