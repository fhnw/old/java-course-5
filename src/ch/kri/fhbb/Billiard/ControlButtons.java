package ch.kri.fhbb.Billiard;

import java.awt.*;
import java.awt.event.*;

public class ControlButtons extends ParentApplet {

	private static final long serialVersionUID = 9010385299595462502L;
	Button b1, b2;

	public void init() {
		b1 = new Button("Start");
		b2 = new Button("Stop");
		b1.addActionListener(new ButtonStart());
		b2.addActionListener(new ButtonStop());
		add(b1);
		add(b2);
	}

	class ButtonStart implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			startPartPools();
		}
	}

	class ButtonStop implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			stopPartPools();
		}
	}

}
