package ch.kri.fhbb.Billiard;

import java.applet.*;
import java.util.*;

public class ParentApplet extends Applet {

	private static final long serialVersionUID = -3962368166216743865L;
	protected static Vector<ParentApplet> partpool_vector = new Vector<ParentApplet>();

	synchronized public void addPartPool(PartPool pp) {
		partpool_vector.addElement(this);
	}

	synchronized protected void stopPartPools() {
		for (int i = 0; i < partpool_vector.size(); i++) {
			((PartPool) partpool_vector.elementAt(i)).stop();
		}
	}

	synchronized protected void startPartPools() {
		for (int i = 0; i < partpool_vector.size(); i++) {
			((PartPool) partpool_vector.elementAt(i)).start();
		}
	}
}
