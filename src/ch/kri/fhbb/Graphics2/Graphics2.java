package ch.kri.fhbb.Graphics2;

import java.applet.*;
import java.awt.*;

public class Graphics2 extends java.applet.Applet {
    private Rectangle appletSize;
    
    public void init () {
        
        // Set the applet's background color
        this.setBackground(new Color(248,248,248));
        
        // Fetch the size of the applet-area
        appletSize = this.getBounds();
        
    }

    public void paint(Graphics g) {
        int left, top, width, height;

        // Width and Height for all objects
        width = appletSize.width / 4;
        height = appletSize.height / 4;

        // erste Reihe
        top = appletSize.height / 8;        
        left = appletSize.width / 8;
        paintText(g, left, top, width, height);
        
        left = 5 * appletSize.width / 8;
        paintImage(g, left, top, width, height);
        
        // zweite Reihe
        top = 5 * appletSize.height / 8;        
        left = appletSize.width / 8;
        paintClippedImage(g, left, top, width, height);
        
        left = 5 * appletSize.width / 8;
        paintCopies(g, left, top, width, height);
    }
  
    private void paintText(Graphics g, int left, int top, int width, int height) {
        Font thisFont = new Font("Serif", Font.ITALIC, 24);
        Color fontColor = new Color(64, 128, 64);
        g.setColor(fontColor);
        g.setFont(thisFont);
        g.drawString("Image Beispiele...",left, top);
    }
    
    private void paintImage(Graphics g, int left, int top, int width, int height) {
        Image thisImage = this.getImage(this.getDocumentBase(), "sunset.gif");
        g.drawImage(thisImage, left, top, this);
    }

        private void paintClippedImage(Graphics g, int left, int top, int width, int height) {
        Polygon thisClip;
        int numPoints = 5;
        int[] xpoints = {0, width/2, width, width/2, 0};
        int[] ypoints = {0, 0, height/2, height, height/2};
        for (int i = 0; i < numPoints; i++) {
            xpoints[i] += left;
            ypoints[i] += top;
        }
        thisClip = new Polygon(xpoints, ypoints, numPoints);
        g.setClip(thisClip);        
        Image thisImage = this.getImage(this.getDocumentBase(), "sunset.gif");
        g.drawImage(thisImage, left, top, this);
        g.setClip(null);
    }

    private void paintCopies(Graphics g, int left, int top, int width, int height) {
        int imageWidth, imageHeight;
        Image thisImage = this.getImage(this.getDocumentBase(), "sunset.gif");
        imageWidth = thisImage.getWidth(this);
        imageHeight = thisImage.getHeight(this);

        g.drawImage(thisImage, left, top, this);
        
        g.copyArea(left, top, imageWidth, imageHeight, 30, 30);
        g.copyArea(left, top, imageWidth+30, imageHeight+30, 60, 60);
    }
}