package ch.kri.fhbb.Gui.v5;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Gui5 extends JFrame implements ActionListener {

  // Variables declaration
  private JTextArea txtBtn1;
  private JTextArea txtBtn2;
  private JTextArea txtBtnBoth;
  private JButton btn1;
  private JButton btn2;

  public static void main(String args[]) {
    new Gui5().setVisible(true);
  }

  // Create new Gui5. We use a complex layout: a border layout
  // containing box-layouts, to demonstrate how layout-managers
  // can be embedded in one another.
  public Gui5() {
    Container cPane = this.getContentPane();

    cPane.setLayout(new GridLayout(3,1,10,10)); // Abstand 10 Pixel zwischen Controls
    
    // die Box-Klasse ist ein Container-Klasse, die BoxLayout verwendet
    Box topBox = Box.createHorizontalBox();

    txtBtn1 = new JTextArea();
    txtBtn1.setEditable(false);
    topBox.add(txtBtn1);

    topBox.add(Box.createHorizontalStrut(20)); // Abstand

    txtBtn2 = new JTextArea();
    txtBtn1.setEditable(false);
    topBox.add(txtBtn2);

    cPane.add(topBox);

    txtBtnBoth = new JTextArea();
    txtBtn1.setEditable(false);

    cPane.add(txtBtnBoth);

    Box bottomBox = Box.createHorizontalBox();

    btn1 = new JButton("Button1");
    bottomBox.add(btn1);

    bottomBox.add(Box.createHorizontalStrut(5)); // Abstand

    btn2 = new JButton("Button2");
    bottomBox.add(btn2);

    cPane.add(bottomBox);

    pack();

    btn1.addActionListener(this);
    btn2.addActionListener(this);

    btn1.addActionListener(new OtherClass(txtBtnBoth));
    btn2.addActionListener(new OtherClass(txtBtnBoth));

    addWindowListener(
      new WindowAdapter() {
        public void windowClosing(java.awt.event.WindowEvent evt) {
          System.exit(0);
        }
      }
    );
  }

    public void actionPerformed(ActionEvent e) {
      if (e.getSource().equals(btn1)) {
        txtBtn1.append("1");
      } else if (e.getSource().equals(btn2)) {
        txtBtn2.append("2");
      }
    }
}

class OtherClass implements ActionListener {
    JTextArea txtArea;
    public OtherClass(JTextArea ta) {
        txtArea = ta;
    }

    public void actionPerformed(ActionEvent e) {
        txtArea.append( ( (JButton) e.getSource()).getText());
    }
}