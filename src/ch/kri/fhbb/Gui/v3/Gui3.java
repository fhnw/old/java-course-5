package ch.kri.fhbb.Gui.v3;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Gui3 extends JFrame implements ActionListener {

    // Variables declaration
    private JLabel lblNumber;
    private JButton btnClick;

    public static void main(String args[]) {
        new Gui3().setVisible(true);
    }

    // Create new Gui3
    public Gui3() {
        getContentPane().setLayout(new GridLayout(2, 1));
        
        lblNumber= new JLabel();
        lblNumber.setText("0");
        getContentPane().add(lblNumber);
        
        btnClick = new JButton();
        btnClick.setText("Click Me!");
        getContentPane().add(btnClick);

        pack();

        btnClick.addActionListener(this);
    }

    // Methode von "ActionListener"
    public void actionPerformed(ActionEvent event) {
      int newValue;

      newValue = Integer.parseInt(lblNumber.getText()) + 1;
      lblNumber.setText(Integer.toString(newValue));
    }
}
