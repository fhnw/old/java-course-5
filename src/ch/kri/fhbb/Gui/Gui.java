package ch.kri.fhbb.Gui;

import java.applet.*;
import java.awt.*;

/**
 *
 * @author  brad
 * @version 
 */
public class Gui extends java.applet.Applet {
    private Rectangle appletSize;
    private Thread myThread;
    
    public void init () {
        // Set the applet's background color
        this.setBackground(new Color(248,248,248));
        
        // Fetch the size of the applet-area
        appletSize = this.getBounds();
    }

    public void paint(Graphics g) {
        int left, top, width, height;

        // Width and Height for all objects
        width = appletSize.width / 4;
        height = appletSize.height / 8;

        // erste Reihe
        top = appletSize.height / 16;        
        left = appletSize.width / 8;
        paintLines(g, left, top, width, height);        
        left = 5 * appletSize.width / 8;
        paintPolyLines(g, left, top, width, height);

        // zweite Reihe
        top = 5 * appletSize.height / 16;
        left = appletSize.width / 8;
        paintRectangles(g, left, top, width, height);
        left = 5 * appletSize.width / 8;
        paintRoundRectangles(g, left, top, width, height);
        
        // dritte Reihe
        top = 9 * appletSize.height / 16;
        left = appletSize.width / 8;
        paint3DRectangles(g, left, top, width, height);
        left = 5 * appletSize.width / 8;
        paintPolygons(g, left, top, width, height);
        
        // vierte Reihe
        top = 13 * appletSize.height / 16;
        left = appletSize.width / 8;
        paintOvals(g, left, top, width, height);
        left = 5 * appletSize.width / 8;
        paintArcs(g, left, top, width, height);
    }
    
    private void paintLines(Graphics g, int left, int top, int width, int height) {
        g.setColor(Color.black);
        g.drawLine(left, top, (left+width), (top+height));        
        g.drawLine((left+width), top, left, (top+height));        
    }
    
    private void paintPolyLines(Graphics g, int left, int top, int width, int height) {
        int numPoints = 5;
        int[] xpoints = {0, 0, width/2, width/2, width};
        int[] ypoints = {0, height/2, 0, height, height/2};
        for (int i = 0; i < numPoints; i++) {
            xpoints[i] += left;
            ypoints[i] += top;
        }
        g.drawPolyline(xpoints, ypoints, numPoints);
    }
    
    private void paintRectangles(Graphics g, int left, int top, int width, int height) {
        g.setColor(Color.green);
        g.fillRect(left, top, width, height);
        g.setColor(Color.cyan);
        g.drawRect(left, top, width, height);
    }
    
    private void paintRoundRectangles(Graphics g, int left, int top, int width, int height) {
        g.setColor(Color.blue);
        g.fillRoundRect(left, top, width, height, 40, 20);
        g.setColor(Color.cyan);
        g.drawRoundRect(left, top, width, height, 40, 20);
    }
    
    private void paint3DRectangles(Graphics g, int left, int top, int width, int height) {
        g.setColor(new Color(160,0,0));
        g.fill3DRect(left, top, width, height, true);
        // wir wollen eine Breite von 4 Pixeln
        g.draw3DRect(left, top, width, height, true);
        g.draw3DRect(left+1, top+1, width-2, height-2, true);
        g.draw3DRect(left+2, top+2, width-4, height-4, true);
        g.draw3DRect(left+3, top+3, width-6, height-6, true);
    }
    
    private void paintOvals(Graphics g, int left, int top, int width, int height) {
        // opaque blue oval in background
        g.setColor(new Color(128,128,255));
        g.fillOval(left, top, width, height);
        g.setColor(new Color(0,0,160));
        g.drawOval(left, top, width, height);
        
        // half-transparent red oval in middle
        g.setColor(new Color(255,128,128,128));
        g.fillOval(left, top, 2*width/3, 2*height/3);
        g.setColor(new Color(160,0,0));
        g.drawOval(left, top, 2*width/3, 2*height/3);
        
        // opaque yellow oval in foreground
        g.setColor(new Color(255,255,128));
        g.fillOval(left+width/3, top+height/3, 2*width/3, 2*height/3);
        g.setColor(new Color(160,160,0));
        g.drawOval(left+width/3, top+height/3, 2*width/3, 2*height/3);
    }

    private void paintArcs(Graphics g, int left, int top, int width, int height) {
        g.setColor(Color.green);
        g.fillArc(left, top, width, height, 45, 120);
        g.drawArc(left, top, width, height, 45, 120);

        g.setColor(Color.blue);
        g.fillArc(left, top, width, height, 45, -120);
        g.drawArc(left, top, width, height, 45, -120);

        g.setColor(Color.red);
        g.fillArc(left, top, width, height, 165, 120);
        g.drawArc(left, top, width, height, 165, 120);
}
    
    private void paintPolygons(Graphics g, int left, int top, int width, int height) {
        int numPoints = 5;
        int[] xpoints = {0, width/2, width, width/2, 0};
        int[] ypoints = {0, 0, height/2, height, height/2};
        for (int i = 0; i < numPoints; i++) {
            xpoints[i] += left;
            ypoints[i] += top;
        }
        g.setColor(new Color(160,0,0));
        g.fillPolygon(xpoints, ypoints, numPoints);
        g.setColor(Color.red);
        g.drawPolygon(xpoints, ypoints, numPoints);
    }
}
