package ch.kri.fhbb.Gui.v0;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Gui_0 extends java.applet.Applet {

    public void init () {
        JFrame frame;
        Container frameContent;
        JLabel lblTemp;
        
        frame = new JFrame("Hello World");
//        frame.addWindowListener( new WindowAdapter() {
//            public void windowClosing(WindowEvent e) {System.exit(0);}
//        });
        
        frameContent = frame.getContentPane();
        frameContent.setLayout(new GridLayout(2,1));
        
        lblTemp = new JLabel("Hello World",JLabel.CENTER);
        frameContent.add(lblTemp);
        
        lblTemp = new JLabel("How are you?",JLabel.CENTER);
        frameContent.add(lblTemp);

        
        frame.setLocation(100,100);
        frame.setSize(300,300);
        frame.setVisible(true);
    }

}
