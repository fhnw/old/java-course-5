package ch.kri.fhbb.Gui.v4;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Gui4 extends JFrame {

    // Variables declaration
    private JLabel lblNumber;
    private JButton btnClick;

    public static void main(String args[]) {
        new Gui4().setVisible(true);
    }

    // Create new Gui3
    public Gui4() {
      getContentPane().setLayout(new GridLayout(2, 1));
        
      lblNumber= new JLabel();
      lblNumber.setText("0");
      getContentPane().add(lblNumber);
        
      btnClick = new JButton();
      btnClick.setText("Click Me!");
      getContentPane().add(btnClick);

      pack();

      btnClick.addMouseListener(
        new MouseAdapter()
        {
          public void mouseClicked(MouseEvent evt) {
            int newValue;

            newValue = Integer.parseInt(lblNumber.getText()) + 1;
            lblNumber.setText(Integer.toString(newValue));
          }
        }
      );

      addWindowListener(
        new WindowAdapter() {
          public void windowClosing(java.awt.event.WindowEvent evt) {
            System.exit(0);
          }
        }
      );
    }
}
