package ch.kri.fhbb.Gui.v1;

import java.awt.*;
import javax.swing.*;

public class Gui_1 extends javax.swing.JApplet {

    // Variables for controls
    private javax.swing.JLabel lblTitle;
    private javax.swing.JTextField txtTitle;
    private javax.swing.JLabel lblName;
    private javax.swing.JTextField txtName;
    private javax.swing.JLabel lblVorname;
    private javax.swing.JTextField txtVorname;
    private javax.swing.JLabel lblAdresse;
    private javax.swing.JTextField txtAdresse;
    private javax.swing.JLabel lblPLZ;
    private javax.swing.JTextField txtPLZ;
    private javax.swing.JLabel lblOrt;
    private javax.swing.JTextField txtOrt;
    private javax.swing.JLabel lblLand;
    private javax.swing.JTextField txtLand;
    private javax.swing.JButton btnShow;
    private javax.swing.JButton btnClose;

    // Frame for displaying results
    private JFrame showFrame;
    
    // Constructor
    public Gui_1() {
        // set layout
        getContentPane().setLayout(new java.awt.GridLayout(8, 2));

        // create and initialize controls, and add them to layout        
        lblTitle = new javax.swing.JLabel();
        lblTitle.setText("Titel");
        getContentPane().add(lblTitle);
        
        txtTitle = new javax.swing.JTextField();
        txtTitle.setText("Herr");
        getContentPane().add(txtTitle);
        
        lblName = new javax.swing.JLabel();
        lblName.setText("Name");
        getContentPane().add(lblName);
        
        txtName = new javax.swing.JTextField();
        txtName.setText("Mustermann");
        getContentPane().add(txtName);
        
        lblVorname = new javax.swing.JLabel();
        lblVorname.setText("Vorname");
        getContentPane().add(lblVorname);
        
        txtVorname = new javax.swing.JTextField();
        txtVorname.setText("Max");
        getContentPane().add(txtVorname);
        
        lblAdresse = new javax.swing.JLabel();
        lblAdresse.setText("Adresse");
        getContentPane().add(lblAdresse);
        
        txtAdresse = new javax.swing.JTextField();
        getContentPane().add(txtAdresse);
        
        lblPLZ = new javax.swing.JLabel();
        lblPLZ.setText("PLZ");
        getContentPane().add(lblPLZ);
        
        txtPLZ = new javax.swing.JTextField();
        getContentPane().add(txtPLZ);
        
        lblOrt = new javax.swing.JLabel();
        lblOrt.setText("Ort");
        getContentPane().add(lblOrt);
        
        txtOrt = new javax.swing.JTextField();
        getContentPane().add(txtOrt);
        
        lblLand = new javax.swing.JLabel();
        lblLand.setText("Land");
        getContentPane().add(lblLand);
        
        txtLand = new javax.swing.JTextField();
        getContentPane().add(txtLand);
        
        btnShow = new javax.swing.JButton();
        btnShow.setText("Anzeigen");
        btnShow.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnShowMouseClicked(evt);
            }
        });
        getContentPane().add(btnShow);
        
        btnClose = new javax.swing.JButton();
        btnClose.setText("Schliessen");
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCloseMouseClicked(evt);
            }
        });
        getContentPane().add(btnClose);
        
    }

    private void btnCloseMouseClicked(java.awt.event.MouseEvent evt) {
        if (showFrame != null) {
            showFrame.setVisible(false);
            showFrame.dispose();
            showFrame=null;
        }
    }

    private void btnShowMouseClicked(java.awt.event.MouseEvent evt) {
        Container frameContent;
        JLabel lblShow;
        
        showFrame = new JFrame("Adresse");
        frameContent = showFrame.getContentPane();
        frameContent.setLayout(new GridLayout(1,1));
        
        lblShow = new JLabel("");
        frameContent.add(lblShow);
        
        lblShow.setText(txtTitle.getText() + " " + txtVorname.getText() + " " + txtName.getText());
        
        showFrame.setLocation(100,100);
        showFrame.setSize(300,100);
        showFrame.setVisible(true);
    }
}