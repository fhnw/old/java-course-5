package ch.kri.fhbb.Gui.v2;

import java.awt.*;
import javax.swing.*;

public class Gui2 extends javax.swing.JFrame {

    // Variables declaration
    private JFrame showFrame;
    private javax.swing.JLabel lblTitel;
    private javax.swing.JTextArea txtTitel;
    private javax.swing.JLabel lblName;
    private javax.swing.JTextArea txtName;
    private javax.swing.JLabel lblVorname;
    private javax.swing.JTextArea txtVorname;
    private javax.swing.JButton btnShow;
    private javax.swing.JButton btnClose;

    public static void main(String args[]) {
        new Gui2().setVisible(true);
    }

    // Create new Gui2
    public Gui2() {
        getContentPane().setLayout(new java.awt.GridLayout(4, 2));
        
        lblTitel = new javax.swing.JLabel();
        lblTitel.setText("Titel");
        getContentPane().add(lblTitel);
        
        txtTitel = new javax.swing.JTextArea();
        getContentPane().add(txtTitel);
        
        lblName = new javax.swing.JLabel();
        lblName.setText("Name");
        getContentPane().add(lblName);
        
        txtName = new javax.swing.JTextArea();
        getContentPane().add(txtName);
        
        lblVorname = new javax.swing.JLabel();
        lblVorname.setText("Vorname");
        getContentPane().add(lblVorname);
        
        txtVorname = new javax.swing.JTextArea();
        getContentPane().add(txtVorname);
        
        btnShow = new javax.swing.JButton();
        btnShow.setText("Anzeigen");
        btnShow.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnShowMouseClicked(evt);
            }
        });
        getContentPane().add(btnShow);

        btnClose = new javax.swing.JButton();
        btnClose.setText("Schliessen");
        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCloseMouseClicked(evt);
            }
        });
        getContentPane().add(btnClose);
        
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });
        
        pack();
    }

    private void btnShowMouseClicked(java.awt.event.MouseEvent evt) {
        Container frameContent;
        JLabel lblShow;
        
        showFrame = new JFrame("Adresse");
        frameContent = showFrame.getContentPane();
        frameContent.setLayout(new GridLayout(1,1));
        
        lblShow = new JLabel("");
        frameContent.add(lblShow);
        
        lblShow.setText(txtTitel.getText() + " " + txtVorname.getText() + " " + txtName.getText());
        
        showFrame.setLocation(100,100);
        showFrame.setSize(300,100);
        showFrame.setVisible(true);
    }

    private void btnCloseMouseClicked(java.awt.event.MouseEvent evt) {
        if (showFrame != null) {
            showFrame.setVisible(false);
            showFrame.dispose();
            showFrame=null;
        }
    }

    /** Exit the Application */
    private void exitForm(java.awt.event.WindowEvent evt) {
        System.exit(0);
    }
}
