package ch.kri.fhbb.RMI;

import java.rmi.*;
import java.rmi.server.*;
import ch.kri.fhbb.RMI.RmiTextObject.*;

public class RmiTextObjectServer extends UnicastRemoteObject implements RemoteTextObject {
    String myText; // Inhalt des Objekts
    
    // Constructor - wir m�ssen die Exception deklarieren, da
    // sie von unserer Superclass verwendet wird.
    public RmiTextObjectServer() throws RemoteException {
        super();
    };
    
    public String getText() throws RemoteException {
        return myText;
    }
    
    public void setText(String newText) throws RemoteException {
        myText = newText;
    }
}
