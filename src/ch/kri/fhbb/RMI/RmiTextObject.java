// Diese Klasse definiert die Schnittstelle des Objekts

package ch.kri.fhbb.RMI;

import java.rmi.*;

public class RmiTextObject {

    /** Die Schnittstelle (interface), die angeboten wird */
    public interface RemoteTextObject extends Remote {
        public String getText() throws RemoteException;
        public void setText(String newText) throws RemoteException;
    }
    
    /** Creates new RmiTextObject */
    public RmiTextObject() {
    }

}
