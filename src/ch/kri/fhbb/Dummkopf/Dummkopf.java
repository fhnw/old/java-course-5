package ch.kri.fhbb.Dummkopf;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Dummkopf extends java.applet.Applet implements ActionListener {

  // diese innere Klasse geh�rt zur Klasse Dummkopf, nicht
  // zu einer Instanz. Ohne "static" w�rde eine bestimme
  // DummkopfException zu einer bestimmten Instanz von
  // Dummkopf geh�ren - und h�tte Zugriff auf alle Instanz-
  // Variablen, Methoden, usw.
  public static class DummkopfException extends Exception {
    public DummkopfException(String text) { super(text); }
  }

  private JFrame appletFrame;
  private Container frameContent;
  private JButton btn;

  public void init() {
    appletFrame= new JFrame("Sei kein Dummkopf!");

    appletFrame.addWindowListener( new WindowAdapter() {
       public void windowClosing(WindowEvent e) {System.exit(0);}
    });
        
    frameContent = appletFrame.getContentPane();
    frameContent.setLayout(new GridLayout(1,1));

    btn = new JButton("Nicht Klicken");
    btn.addActionListener(this);
    frameContent.add(btn);

    appletFrame.setLocation(100,100);
    appletFrame.setSize(300,300);
    appletFrame.setVisible(true);
  }

  public void actionPerformed(ActionEvent event) {
    String cmd = event.getActionCommand();
    if (cmd.equals("Nicht Klicken")) {
      ShowDialog errDialog = new ShowDialog("Title", "Hi there");
    }
  }

  class ShowDialog extends javax.swing.JFrame {
    public ShowDialog(String title, String message) {
            JFrame errorDialog = new JFrame(title);
            Container frameContent = errorDialog.getContentPane();
            frameContent.setLayout(new GridLayout(1,1));
            JLabel lblError = new JLabel("");
            frameContent.add(lblError);
            lblError.setText(message);
            errorDialog.pack();
            errorDialog.setLocation(100,100);
            errorDialog.setVisible(true);
    }
  }
}