package ch.kri.fhbb.BinaryFileCopy;

import java.awt.*;
import javax.swing.*;
import java.io.*;

public class BinaryFileCopy extends JFrame {
	// GUI components
	private javax.swing.JLabel jLabel1;
	private javax.swing.JTextField txtFileIn;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JTextField txtFileOut;
	private javax.swing.JButton btnCopy;
	private javax.swing.JButton btnClose;

	public static void main(String args[]) {
		new BinaryFileCopy();
	}

	public BinaryFileCopy() {
		this.setTitle("Binary file copy");
		Container pane = getContentPane();
		pane.setLayout(new java.awt.GridLayout(3, 2));

		jLabel1 = new javax.swing.JLabel("Input File");
		pane.add(jLabel1);
		txtFileIn = new javax.swing.JTextField();
		txtFileIn.setPreferredSize(new Dimension(150, 0));
		pane.add(txtFileIn);

		jLabel2 = new javax.swing.JLabel("Output File");
		pane.add(jLabel2);
		txtFileOut = new javax.swing.JTextField();
		pane.add(txtFileOut);

		// Copy button
		btnCopy = new javax.swing.JButton("Copy");
		btnCopy.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnCopyMouseClicked(evt);
			}
		});
		pane.add(btnCopy);

		// Quit button
		btnClose = new javax.swing.JButton("Quit");
		btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnCloseMouseClicked(evt);
			}
		});
		pane.add(btnClose);

		// Window closing event
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				btnCloseMouseClicked(null);
			}
		});

		this.pack();
		this.setVisible(true);
	}

	private void btnCopyMouseClicked(java.awt.event.MouseEvent evt) {
		ShowDialog errDialog;

		String fileNameIn = txtFileIn.getText();
		String fileNameOut = txtFileOut.getText();

		// Create two file objects
		File fileIn = new File(fileNameIn);
		File fileOut = new File(fileNameOut);

		// Check the input file
		if (!fileIn.exists()) { // does not exist
			errDialog = new ShowDialog("Error", "'" + fileNameIn + "' does not exist!");
			return;
		}
		if (!fileIn.isFile()) { // is a directory
			errDialog = new ShowDialog("Error", "'" + fileNameIn + "' is not a file!");
			return;
		}
		if (!fileIn.canRead()) { // Not readable
			errDialog = new ShowDialog("Error", "'" + fileNameIn + "' cannot be read!");
			return;
		}

		// Check the output file
		if (fileOut.exists()) { // exists - must be writeable
			if (!fileOut.canWrite()) {
				errDialog = new ShowDialog("Error", "'" + fileNameOut + "' cannot be written!");
				return;
			}
		} else {
			String directoryName = fileOut.getParent();
			if (directoryName == null) { // No path, so use the current directory
				directoryName = System.getProperty("user.dir");
			}
			File directory = new File(directoryName);
			if (!directory.exists()) { // does not exist
				errDialog = new ShowDialog("Error", "Directory '" + directoryName + "' does not exist!");
				return;
			}
			if (!directory.canWrite()) { // Not writeable
				errDialog = new ShowDialog("Error", "Directory '" + directoryName + "' cannot be written!");
				return;
			}
		}

		// Finally, everything is ok!
		FileInputStream streamIn = null;
		FileOutputStream streamOut = null;
		try {
			// Create our input and output streams
			streamIn = new FileInputStream(fileIn);
			streamOut = new FileOutputStream(fileOut);
			byte[] buffer = new byte[4096];
			int bytesRead;

			// In a loop: read, write, read, write - until done
			bytesRead = streamIn.read(buffer); // the Read method returns the number of bytes
			while (bytesRead != -1) {
				streamOut.write(buffer, 0, bytesRead); // buffer, begin, length
				bytesRead = streamIn.read(buffer);
			}
		} catch (Exception e) {
			errDialog = new ShowDialog("Exception", e.toString());
		} finally { // Whether or not there was an error, we must close the streams
			if (streamIn != null) try {
				streamIn.close();
			} catch (IOException e2) {
			}
			if (streamOut != null) try {
				streamOut.close();
			} catch (IOException e3) {
			}
		}
	}

	private void btnCloseMouseClicked(java.awt.event.MouseEvent evt) {
		System.exit(0);
	}

	class ShowDialog extends JFrame {
		public ShowDialog(String title, String message) {
			JFrame errorDialog = new JFrame(title);
			Container frameContent = errorDialog.getContentPane();
			frameContent.setLayout(new GridLayout(1, 1));
			JLabel lblError = new JLabel("");
			frameContent.add(lblError);
			lblError.setText(message);
			errorDialog.pack();
			errorDialog.setLocation(100, 100);
			errorDialog.setVisible(true);
		}
	}

}
