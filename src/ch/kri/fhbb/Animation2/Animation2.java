package ch.kri.fhbb.Animation2;

import java.applet.*;
import java.awt.*;

public class Animation2 extends javax.swing.JApplet implements Runnable {
	private int FrameRate = 10;

	private Color fillColor, borderColor, textColor;
	private Font textFont;
	private FontMetrics fontInfo;
	private Image animationImage;
	private Rectangle appletSize;
	private Thread myThread;

	private AnimatedImage animation;
	private volatile boolean running; // if false, thread should temporarily stop
	private volatile boolean alive; // if false, the applet is being destroyed
	private int animationStep = 0; // controls simple animation effects
	private final int animationMax = 50;
	private int animationDelta = 100 / FrameRate; // contrhols simple animation effects

	public void destroy() {
		animation.destroy();
	}

	public void init() {
		// create initial values for the colors, font and animation-image
		fillColor = new Color(128, 255, 128);
		borderColor = new Color(0, 160, 0);
		textColor = new Color(0, 0, 192);

		textFont = new Font("Serif", Font.BOLD, 20);
		animationImage = this.getImage(this.getDocumentBase(), "ship.gif");

		// Set the applet's background color
		this.setBackground(new Color(248, 248, 248));

		// Fetch the size of the applet-area
		appletSize = this.getBounds();

		// Create the animated image
		animation = new AnimatedImage(animationImage, appletSize, this);

		running = false;
		alive = true;

		myThread = new Thread(this);
		myThread.setName("repaint");
		myThread.start();
	}

	public void paint(Graphics g) {
		int left, top, width, height;

		animation.paint(g);

		// Adjust the animation step (range -10 to 10)
		if (((animationDelta > 0) & (animationStep >= animationMax))
				| ((animationDelta < 0) & (animationStep <= (0 - animationMax)))) {
			animationDelta = 0 - animationDelta;
		}
		animationStep += animationDelta;

		// Width and Height for all objects
		width = appletSize.width / 4;
		height = appletSize.height / 4;

		// erste Reihe
		top = appletSize.height / 8;
		left = appletSize.width / 8;
		paint3DRectangles(g, left, top, width, height);
		left = 5 * appletSize.width / 8;
		paintPolygons(g, left, top, width, height);

		// vierte Reihe
		top = 5 * appletSize.height / 8;
		left = appletSize.width / 8;
		paintOvals(g, left, top, width, height);
		left = 5 * appletSize.width / 8;
		paintArcs(g, left, top, width, height);
	}

	private void paint3DRectangles(Graphics g, int left, int top, int width, int height) {
		int realWidth = width + animationStep;

		g.setColor(new Color(160, 0, 0));
		g.fill3DRect(left, top, realWidth, height, true);
		// wir wollen eine Breite von 4 Pixeln
		g.draw3DRect(left, top, realWidth, height, true);
		g.draw3DRect(left + 1, top + 1, realWidth - 2, height - 2, true);
		g.draw3DRect(left + 2, top + 2, realWidth - 4, height - 4, true);
		g.draw3DRect(left + 3, top + 3, realWidth - 6, height - 6, true);
	}

	private void paintOvals(Graphics g, int left, int top, int width, int height) {
		// opaque blue oval in background
		g.setColor(new Color(128 - animationStep, 128 + animationStep, 255));
		g.fillOval(left, top, width, height);
		g.setColor(new Color(0, 0, 160));
		g.drawOval(left, top, width, height);

		// half-transparent red oval in middle
		g.setColor(new Color(255, 128 + animationStep, 128 - animationStep, 128));
		g.fillOval(left, top, 2 * width / 3, 2 * height / 3);
		g.setColor(new Color(160, 0, 0));
		g.drawOval(left, top, 2 * width / 3, 2 * height / 3);

		// opaque yellow oval in foreground
		g.setColor(new Color(255, 255, 128 + animationStep));
		g.fillOval(left + width / 3, top + height / 3, 2 * width / 3, 2 * height / 3);
		g.setColor(new Color(160, 160, 0));
		g.drawOval(left + width / 3, top + height / 3, 2 * width / 3, 2 * height / 3);
	}

	private void paintArcs(Graphics g, int left, int top, int width, int height) {
		g.setColor(Color.green);
		g.fillArc(left, top, width, height, 45 + animationStep, 120);
		g.drawArc(left, top, width, height, 45 + animationStep, 120);

		g.setColor(Color.blue);
		g.fillArc(left, top, width, height, 45 + animationStep, -120);
		g.drawArc(left, top, width, height, 45 + animationStep, -120);

		g.setColor(Color.red);
		g.fillArc(left, top, width, height, 165 + animationStep, 120);
		g.drawArc(left, top, width, height, 165 + animationStep, 120);
	}

	private void paintPolygons(Graphics g, int left, int top, int width, int height) {
		int numPoints = 5;
		int[] xpoints = { 0, width / 2, width, width / 2, 0 };
		int[] ypoints = { 0, 0, height / 2, height, height / 2 };
		for (int i = 0; i < numPoints; i++) {
			xpoints[i] += left + (animationStep * i) / 15;
			ypoints[i] += top - (animationStep * i) / 15;
		}
		g.setColor(new Color(160, 0, 0));
		g.fillPolygon(xpoints, ypoints, numPoints);
		g.setColor(Color.red);
		g.drawPolygon(xpoints, ypoints, numPoints);
	}

	public void run() {
		while (alive) {
			if (running) {
				repaint();
			}

			try {
				Thread.sleep(1000 / FrameRate);
			} catch (InterruptedException e) {
			}
		}
	}

	public void start() {
		running = true;
		animation.start();
	}

	public void stop() {
		running = false;
		animation.stop();
	}
}

class AnimatedImage implements Runnable {
	private int xPos, yPos;
	private int xVel, yVel;
	private Rectangle bounds;
	private Image image;
	private Applet mainApplet;
	private boolean running;
	private boolean alive;
	private Thread myThread;

	public AnimatedImage(Image image, Rectangle bounds, Applet mainApplet) {
		this.bounds = bounds;
		this.image = image;
		this.mainApplet = mainApplet;

		xPos = (bounds.width - image.getWidth(mainApplet)) / 2;
		yPos = (bounds.height - image.getHeight(mainApplet)) / 2;

		xVel = (int) ((Math.random() * 10.0) / 2.0);
		yVel = (int) ((Math.random() * 10.0) / 2.0);

		running = false;
		alive = true;

		myThread = new Thread(this);
		myThread.setName("animation");
		myThread.start();
	}

	public void destroy() {
		alive = false;
	}

	public void paint(Graphics g) {
		g.drawImage(image, xPos, yPos, mainApplet);
	}

	public void run() {
		int newX, newY;

		while (alive) {
			if (running) {
				newX = xPos + xVel;
				if ((newX < 0) | (newX > (bounds.width - image.getWidth(mainApplet)))) {
					xVel = 0 - xVel;
				}
				xPos += xVel;

				newY = yPos + yVel;
				if ((newY < 0) | (newY > (bounds.height - image.getHeight(mainApplet)))) {
					yVel = 0 - yVel;
				}
				yPos += yVel;
			}

			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
		}
	}

	public void start() {
		running = true;
	}

	public void stop() {
		running = false;
	}
}