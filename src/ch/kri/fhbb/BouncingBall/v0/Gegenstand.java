package ch.kri.fhbb.BouncingBall.v0;

abstract class Gegenstand {
	public java.awt.Color color;

	public int xPosition, yPosition;

	public double dichte;

	public double gewicht() {
		return dichte * flaeche();
	}

	abstract public double flaeche();
}
