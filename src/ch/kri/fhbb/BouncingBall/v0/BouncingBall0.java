package ch.kri.fhbb.BouncingBall.v0;

import java.applet.*;
import java.awt.*;

public class BouncingBall0 extends Applet {
	private static final long serialVersionUID = 4262179613793386005L;
	private Kreis kreis1;
    
    public void init() {
        kreis1 = new Kreis(10);
    }
    
    public void paint (Graphics pGraphics) {
        kreis1.paint(pGraphics);
    }
}
