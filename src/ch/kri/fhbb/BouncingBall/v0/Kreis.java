package ch.kri.fhbb.BouncingBall.v0;

import java.awt.Graphics;

class Kreis extends Gegenstand {
	private static final double PI = 3.14159;

	private double radius;

	public Kreis(double radius) {
		this.radius = radius;
		dichte = 1.0;
		color = java.awt.Color.blue;
		xPosition = 25;
		yPosition = 50;
	}

	public double flaeche() {
		return (PI * radius * radius);
	}

	public void paint(Graphics pGraphics) {
		pGraphics.setColor(color);
		pGraphics.fillOval(xPosition, yPosition, (int) (2 * radius),
				(int) (2 * radius));
		pGraphics.drawString("Gewicht " + gewicht(), (int) (xPosition + 2
				* radius + 5), (int) (yPosition + 2 * radius));
	}
}
