package ch.kri.fhbb.BouncingBall.v2;

import java.awt.*;

public class BouncingBall2 extends java.applet.Applet implements Runnable {
    public int yMax;
    protected int xMax;
    private Thread thread;
    private volatile boolean running; // a variable indicating that the thread should stop.
                                      // "volatile" means that this variable is accessed from
                                      // multiple threads, and may not be cached by the JVM

    private final int numBalls = 3;
    private Kreis[] ball = new Kreis[numBalls]; // the balls that we are working with

    public void start() {
        for (int i = 0; i < numBalls; i++) {
            Color objColor;
            switch (i % 3) {
                case 0:
                    objColor = Color.blue;
                    break;
                case 1:
                    objColor = Color.green;
                    break;
                case 2:
                    objColor = Color.red;
                    break;
                default:
                    objColor = Color.black;
            }
        
            ball[i] = new Kreis(this, "ball-" + i, objColor, i*10, i*20, i+1, i+2, 10);
        }
        running = true;
        thread = new Thread(this);
        thread.setName("BB-repaint");
        thread.start();
    }
    
    public void stop() {
        for (int i = 0; i < numBalls; i++) {
            ball[i].stop();
        }
        
        running = false;
    }
    
    public void paint(Graphics pGraphics) {
        for (int i = 0; i < numBalls; i++) {
            ball[i].paint(pGraphics);
        }
    }
    
    public void run() {
        while (running) { // continue until we are told to stop
            repaint();
            
            Rectangle bounds = getBounds();
            yMax = bounds.height;
            xMax = bounds.width;
            
            try { thread.sleep(50); }
            catch(InterruptedException e) {}
        }
        thread = null; // dereference the thread - not really necessary, but a nicety
    }    
}

abstract class Gegenstand implements Runnable {
    protected String name;
    protected java.awt.Color color;
    protected int xPosition, yPosition;
    protected int xGeschwindigkeit, yGeschwindigkeit;
    protected double dichte;
    protected BouncingBall2 parent;

    protected Thread objectThread;
    protected volatile boolean running;

    public double gewicht() {
        return dichte * flaeche();
    }
    
    Gegenstand(BouncingBall2 parent, String name, Color color, int xPos, int yPos, int xGeschw, int yGeschw) {
        this.parent = parent;
        this.name = name;
        this.color = color;
        this.xPosition = xPos;
        this.yPosition = yPos;
        this.xGeschwindigkeit = xGeschw;
        this.yGeschwindigkeit = yGeschw;
        this.dichte = 1.0;
    }
    
    public void start() {
        running = true;
        
        // initialize the thread for this object
        objectThread = new Thread(this);
        objectThread.setName("BB-" + this.name);
        objectThread.start();
    }
    
    public void stop() {
        running = false;
    }
    
    abstract public double flaeche();
    abstract public void run();
}

class Kreis extends Gegenstand implements Runnable {
    private static final double PI = 3.14159;
    
    private double radius;

    public Kreis(BouncingBall2 parent, String name, Color color, int xPos, int yPos, int xGeschw, int yGeschw, int radius) {
        // perform all initialization for this object
        super(parent, name, color, xPos, yPos, xGeschw, yGeschw); // initialize the super-class
        this.radius = radius;                             // specific to "Kreis"
        
        // after initialization, start the thread for this object
        super.start();
    }
    
    public double flaeche() {
        return (PI * radius * radius);
    }

    public void paint(Graphics pGraphics) {
            pGraphics.setColor(color);
            pGraphics.fillOval(xPosition, yPosition, (int) (2*radius), (int) (2*radius));
            pGraphics.drawString(name, (int) (xPosition + 2 * radius + 5), (int) (yPosition + 2 * radius));
    }
    
    public void run() {
        while(running) { // continue until we are told to stop
            
            // vertical position
            if (yGeschwindigkeit < 0) {
                if ((yPosition + yGeschwindigkeit) < 0) yGeschwindigkeit = 0 - yGeschwindigkeit;
            }
            else {
                if ((yPosition + yGeschwindigkeit + 2*radius) > parent.yMax) yGeschwindigkeit = 0 - yGeschwindigkeit;
            }
            yPosition += yGeschwindigkeit;
            
            // horizontal position
            if (xGeschwindigkeit < 0) {
                if ((xPosition + xGeschwindigkeit) < 0) xGeschwindigkeit = 0 - xGeschwindigkeit;
            }
            else {
                if ((xPosition + xGeschwindigkeit + 2*radius) > parent.xMax) xGeschwindigkeit = 0 - xGeschwindigkeit;
            }
            xPosition += xGeschwindigkeit;

            try { objectThread.sleep(50); }
            catch(InterruptedException e) {}
        }
        
        objectThread = null; // dereference the thread - not really necessary, but a nicety
    }
    
}