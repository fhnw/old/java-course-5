package ch.kri.fhbb.BouncingBall.v1;

import java.awt.Color;

abstract class Gegenstand implements Runnable {
    protected String name;
    protected java.awt.Color color;
    protected int xPosition, yPosition;
    protected int yGeschwindigkeit;
    protected double dichte;

    protected Thread objectThread;
    protected volatile boolean running;

    public double gewicht() {
        return dichte * flaeche();
    }
    
    Gegenstand(String name, Color color, int xPos, int yPos, int yGeschw) {
        this.name = name;
        this.color = color;
        this.xPosition = xPos;
        this.yPosition = yPos;
        this.yGeschwindigkeit = yGeschw;
        this.dichte = 1.0;
    }
    
    public void start() {
        running = true;
        
        // initialize the thread for this object
        objectThread = new Thread(this);
        objectThread.setName("BB-" + this.name);
        objectThread.start();
    }
    
    public void stop() {
        running = false;
    }
    
    abstract public double flaeche();
    abstract public void run();
}