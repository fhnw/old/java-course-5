package ch.kri.fhbb.BouncingBall.v1;

import java.applet.*;
import java.awt.*;

public class BouncingBall1 extends Applet implements Runnable {
	private static final long serialVersionUID = -5187205438290886602L;
	private Thread thread;
    private volatile boolean running; // a variable indicating that the thread should stop.
                                      // "volatile" means that this variable is accessed from
                                      // multiple threads, and may not be cached by the JVM
    private Kreis ball;

    public void start() {
        ball = new Kreis("ball-0", Color.blue, 10, 10, 2, 10);
        
        running = true;
        thread = new Thread(this);
        thread.setName("BB-repaint");
        thread.start();
    }
    
    public void stop() {
        ball.stop();
        running = false;
    }
    
    public void paint(Graphics pGraphics) {
        ball.paint(pGraphics);
    }
    
    public void run() {
        while (running) { // continue until we are told to stop
            repaint();
            
            try { Thread.sleep(50); }
            catch(InterruptedException e) {}
        }
        thread = null; // dereference the thread - not really necessary, but a nicety
    }    
}



