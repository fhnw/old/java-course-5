package ch.kri.fhbb.BouncingBall.v1;

import java.awt.Color;
import java.awt.Graphics;

class Kreis extends Gegenstand implements Runnable {
    private static final double PI = 3.14159;
    
    private double radius;

    public Kreis(String name, Color color, int xPos, int yPos, int yGeschw, int radius) {
        // perform all initialization for this object
        super(name, color, xPos, yPos, yGeschw); // initialize the super-class
        this.radius = radius;                             // specific to "Kreis"
        
        // after initialization, start the thread for this object
        super.start();
    }
    
    public double flaeche() {
        return (PI * radius * radius);
    }

    public void paint(Graphics pGraphics) {
            pGraphics.setColor(color);
            pGraphics.fillOval(xPosition, yPosition, (int) (2*radius), (int) (2*radius));
            pGraphics.drawString(name, (int) (xPosition + 2 * radius + 5), (int) (yPosition + 2 * radius));
    }
    
    public void run() {
        while(running) { // continue until we are told to stop
            // vertical position
            if (yGeschwindigkeit < 0) {
                if ((yPosition + yGeschwindigkeit) < 0) yGeschwindigkeit = 0 - yGeschwindigkeit;
            }
            else {
                if ((yPosition + yGeschwindigkeit) > 100) yGeschwindigkeit = 0 - yGeschwindigkeit;
            }
            yPosition += yGeschwindigkeit;

            try { Thread.sleep(50); }
            catch(InterruptedException e) {}
        }
        
        objectThread = null; // dereference the thread - not really necessary, but a nicety
    }
    
}
