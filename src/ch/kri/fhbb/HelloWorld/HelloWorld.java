/*
 * Applet.java
 *
 * Created on October 18, 2001, 8:32 PM
 */

package ch.kri.fhbb.HelloWorld;

/**
 * 
 * @author brad
 * @version
 */
public class HelloWorld extends java.applet.Applet {
	private static final long serialVersionUID = -3281578947709962730L;

	public void paint(java.awt.Graphics g) {
		g.drawString("Hello World", 25, 50);
	}

}
