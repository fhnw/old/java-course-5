package ch.kri.fhnw.WebServer2;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;

// Thread class that listens for connections. When a request arrives, it creates
// an independent thread to communicate with the client. This is a cleaner solution
// than the implicit anonymous class used in WebServer1.
public class Listener extends Thread {
	WebServer2 parent;
	ServerSocket listener;
	int port;
	volatile boolean stopThread = false;

	public Listener(WebServer2 parent, int port) throws IOException {
		super("Listener:" + port); // name of our thread
		this.parent = parent;
		listener = new ServerSocket(port);
		listener.setSoTimeout(0); // never time out
		this.port = port;
	}

	public void stopListening() {
		if (!this.stopThread) {
			this.stopThread = true;
			try {
				listener.close();
			} catch (IOException e) {
			}
		}
	}

	public void run() {
		parent.log("Starting listener");
		while (!stopThread) {
			try {
				Socket client = listener.accept(); // wait for a request
				parent.addConnection(client); // process the request
			} catch (InterruptedIOException e) {
				parent.log("Listener thread interrupted");
			}
			catch (IOException e) {
				parent.log("IOException in run " + e.toString());
			}
		}
		parent.log("Stopped listener");
	}
}
