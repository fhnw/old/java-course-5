package ch.kri.fhnw.WebServer2;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;

public class ClientConnection extends Thread {
	WebServer2 parent;
	Socket client;
	String clientAddress;

	public ClientConnection(WebServer2 parent, Socket client) {
		super("ClientConnection:" + client.getInetAddress().getHostAddress() + ":" + client.getPort());
		this.parent = parent;
		clientAddress = client.getInetAddress().getHostAddress() + ":" + client.getPort();
		parent.log("ClientConnection:" + clientAddress);
		this.client = client;
	}

	public void run() {
		try {
			// Create input and output streams to talk to the client
			BufferedReader inClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
			BufferedOutputStream outClient = new BufferedOutputStream(client.getOutputStream());
			Writer outClientWriter = new OutputStreamWriter(outClient);

			// Read request from client and parse out the file name from the first line
			String inString;
			String strFileName = "xxxx.html";
			while ((inString = inClient.readLine()) != null) {
				if (inString.length() == 0) {
					break; // an empty line is the end of the request
				} else {
					if (inString.regionMatches(0, "GET ", 0, 4)) {
						int fileNameEnd = inString.indexOf(" ", 4);
						strFileName = "c:" + inString.substring(4, fileNameEnd);
					}
				}
				// outClient.print(inString + "\n");
			}

			File fileIn = new File(strFileName);

			if (!fileIn.exists()) { // 404
				// Send our reply using HTTP 1.0
				outClientWriter.write("HTTP/1.0 404 \n"); // Version and status
				outClientWriter.write("Content-Type: text/plain\n");
				outClientWriter.write("\n");
				outClientWriter.write("HTTP 404 - File not found: " + strFileName + "\n");
			} else {
				int suffixStart = strFileName.indexOf(".") + 1;
				String fileSuffix = strFileName.substring(suffixStart, strFileName.length());
				String contentType = "text/plain";
				if (fileSuffix.equals("jpg") | fileSuffix.equals("jpeg")) {
					contentType = "image/jpeg";
				} else if (fileSuffix.equals("gif")) {
					contentType = "image/gif";
				} else if (fileSuffix.equals("html") | fileSuffix.equals("htm")) {
					contentType = "text/html";
				}

				BufferedInputStream inFromFile = null;
				// Send our reply using HTTP 1.0
				outClientWriter.write("HTTP/1.0 200 \n"); // Version and status
				outClientWriter.write("Content-Type: " + contentType + "\n");
				outClientWriter.write("\n");
				outClientWriter.flush();

				try {
					inFromFile = new BufferedInputStream(new FileInputStream(fileIn));

					// In einer Schleife: lesen, schreiben - bis wir fertig sind
					byte buffer[] = new byte[4096];
					int bytesRead;
					while ((bytesRead = inFromFile.read(buffer)) > -1) {
						outClient.write(buffer, 0, bytesRead);
					}
				} catch (Exception e) {
					parent.log(e.toString());
				} finally { // Ob Fehler oder nicht, wir mussen die Streams schliessen
					if (inFromFile != null) try {
						inFromFile.close();
					} catch (IOException e2) {
					}
				}
			}

			outClient.close();
			inClient.close();
			client.close();
		} catch (Exception e) {
			parent.log(e.toString());
		} finally {
			parent.connections.remove(this);
			parent.log("Connection closed to " + clientAddress);
		}
	}
}
