package ch.kri.fhnw.SimplePaint;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class DrawingTool extends JButton  implements ActionListener {
	public static final Point hotSpot = new Point(0,0); // cursor hotspot 
	Paint parent;
	Toolkit toolkit = Toolkit.getDefaultToolkit(); // needed for cursors
	
	public DrawingTool(Paint parent, Icon icon) {
		super();
		this.parent = parent;
		this.setPreferredSize(new Dimension(20, 20));
		this.setIcon(icon);
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		ImageIcon ourIcon = (ImageIcon) this.getIcon();
		parent.setTool(ourIcon);
		Image img = ourIcon.getImage();
		Cursor cur = toolkit.createCustomCursor(img, hotSpot, "cursor name");
		parent.setCursor(cur);
	}
}
