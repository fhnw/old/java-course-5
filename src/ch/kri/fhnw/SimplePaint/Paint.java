package ch.kri.fhnw.SimplePaint;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Paint extends JFrame {
	private Canvas canvas;
	private Box tools;
	private Color currentColor;
	private Icon currentTool;
	Icon lineTool;
	Icon rectTool;
	
	public static void main(String[] args) {
		new Paint();
	}

	private Paint() {
		// Create icons
		lineTool = createImageIcon(this.getClass(), "line_icon.gif");
		rectTool = createImageIcon(this.getClass(), "rectangle_icon.gif");
		
		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());

		tools = Box.createVerticalBox();
		tools.add(new ColorPot(this, Color.RED));
		tools.add(new ColorPot(this, Color.BLUE));
		tools.add(new ColorPot(this, Color.GREEN));
		tools.add(new ColorPot(this, Color.WHITE));
		tools.add(new ColorPot(this, Color.BLACK));
		tools.add(Box.createVerticalStrut(10));
		tools.add(new DrawingTool(this, lineTool));
		tools.add(new DrawingTool(this, rectTool));
		tools.add(Box.createVerticalGlue());
		pane.add(tools, BorderLayout.WEST);

		canvas = new Canvas(this);
		pane.add(canvas, BorderLayout.CENTER);
		
    addWindowListener(
        new WindowAdapter() {
          public void windowClosing(WindowEvent evt) {
            System.exit(0);
          }
        }
      );
		
    // set defaults
    currentColor = Color.black;
    currentTool = null;
    
		pack();
		setVisible(true);		
	}

	Color getColor() {
		return currentColor;
	}
	void setColor(Color color) {
		currentColor = color;
	}
	Icon getTool() {
		return currentTool;
	}
	void setTool(Icon tool) {
		currentTool = tool;
	}
	
	/** Returns an ImageIcon, or null if the path was invalid.
	 * (adapted from http://java.sun.com/docs/books/tutorial/uiswing/misc/icon.html)
	 * 
	 * @param callingClass - the class associated with the image we are to convert to an icon
	 * @param path - the path to the image, relative to the class
	 * #param description - description of the icon (optional)
	*/
  public static javax.swing.ImageIcon createImageIcon(Class<?> callingClass, String path, String... description) {
    java.net.URL imgURL = callingClass.getResource(path);
    if (imgURL != null) {
    	if (description.length > 0)
        return new javax.swing.ImageIcon(imgURL, description[0]);
    	else
        return new javax.swing.ImageIcon(imgURL);
    } else {
        System.err.println("Couldn't find resource: " + path);
        return null;
    }
  }  	
}
