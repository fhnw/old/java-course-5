package ch.kri.fhnw.SimplePaint;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class Canvas extends java.awt.Canvas implements MouseListener {
	private Paint parent;
	private Point mousePress = null;
	private ArrayList<Shape> lines = new ArrayList<Shape>();
	private ArrayList<Shape> rects = new ArrayList<Shape>();
	
	public Canvas(Paint parent) {
		super();
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(300, 300));
		this.parent = parent;
		this.addMouseListener(this);
	}
	
	@Override
	public void paint(Graphics g) {
		for (Shape line : lines) {
			g.setColor(line.color);
			g.drawLine(line.start.x, line.start.y, line.end.x, line.end.y);
		}
		for (Shape rect : rects) {
			g.setColor(rect.color);
			int width = rect.end.x - rect.start.x;
			int height = rect.end.y - rect.start.y;
			g.drawRect(rect.start.x, rect.start.y, width, height);
		}
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		mousePress = evt.getPoint();
	}
	@Override
	public void mouseReleased(MouseEvent evt) {
		Point mouseUp = evt.getPoint();
		Color color = parent.getColor();
		Graphics g = this.getGraphics();
		if (mousePress != null & g != null) {
			g.setColor(color);
			if (parent.getTool() == null) {
				// do nothing
			} else if (parent.getTool().equals(parent.lineTool)) {
				lines.add(new Shape(mousePress, mouseUp, color));
				g.drawLine(mousePress.x, mousePress.y, mouseUp.x, mouseUp.y);
			} else if (parent.getTool().equals(parent.rectTool)) {
				rects.add(new Shape(mousePress, mouseUp, color));
				int width = mouseUp.x-mousePress.x;
				int height = mouseUp.y - mousePress.y;
				g.drawRect(mousePress.x, mousePress.y, width, height);
			}
		}
	}
	@Override
	public void mouseClicked(MouseEvent evt) {
	}
	@Override
	public void mouseExited(MouseEvent evt) {
	}
	@Override
	public void mouseEntered(MouseEvent evt) {
	}

	private class Shape {
		private Point start;
		private Point end;
		private Color color;

		private Shape(Point start, Point end, Color color) {
			this.start = start;
			this.end = end;
			this.color = color;
		}
	}
}
