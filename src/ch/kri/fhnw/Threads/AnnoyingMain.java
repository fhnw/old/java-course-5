package ch.kri.fhnw.Threads;

import java.awt.Container;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AnnoyingMain extends JFrame {
	private AnnoyingThread annoyance;
	private JButton threadButton;
	private Canvas canvas;

	public static void main(String[] args) {
		new AnnoyingMain();
	}

	public AnnoyingMain() {
		super();
		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());

		JPanel j = new JPanel();
		j.setPreferredSize(new Dimension(40, 40));
		j.setBackground(new Color(128, 128, 128, 50));
		pane.add(j, BorderLayout.NORTH);
		j = new JPanel();
		j.setPreferredSize(new Dimension(40, 40));
		j.setBackground(new Color(128, 128, 128, 50));
		pane.add(j, BorderLayout.EAST);
		j = new JPanel();
		j.setPreferredSize(new Dimension(40, 40));
		j.setBackground(new Color(128, 128, 128, 50));
		pane.add(j, BorderLayout.WEST);
		j = new JPanel();
		j.setPreferredSize(new Dimension(40, 40));
		j.setBackground(new Color(128, 128, 128, 50));
		pane.add(j, BorderLayout.SOUTH);

		threadButton = new JButton("Start annoying me!");
		pane.add(threadButton, BorderLayout.CENTER);
		threadButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				doIt();
			}
		});
		// Handle window-closing event
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				System.exit(0);
			}
		});

		pack();
		setVisible(true);
	}

	private void doIt() {
		if (annoyance == null) {
			annoyance = new AnnoyingThread(this, "annoyer");
			annoyance.start();
			threadButton.setText("Stop annoying me!");
		} else {
			annoyance.pleaseStop();
			annoyance.interrupt(); // optional
		}
	}
}
