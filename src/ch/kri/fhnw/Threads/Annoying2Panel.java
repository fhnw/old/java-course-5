package ch.kri.fhnw.Threads;

import java.awt.Color;

import javax.swing.JPanel;

public class Annoying2Panel extends JPanel implements Runnable {
	String name;
	Thread thread;
	Boolean stopThread;
	int sleepTime;
	
	Annoying2Panel(String name, int sleepTime) {
		super();
		this.name = name;
		this.sleepTime = sleepTime;
	}
	
	@Override
	public void run() {
		while (!stopThread) {
			if (this.getBackground().equals(Color.WHITE)) {
				this.setBackground(Color.BLACK);
			} else {
				this.setBackground(Color.WHITE);
			}
			this.repaint();
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
			}
		}
	}

	boolean isAlive() {
		if (thread == null) {
			return false;
		} else {
			return thread.isAlive();
		}
	}
	
	void start() {
		thread = new Thread(this, name);
		stopThread = false;
		System.out.println("starting thread " + name);
		thread.start();
	}
	
	void stop() {
		stopThread = true;
		thread.interrupt();
	}
}
