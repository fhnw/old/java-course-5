package ch.kri.fhnw.Threads;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Annoying2Main extends JFrame {
	private ArrayList<Annoying2Panel> panels;
	private JButton threadButton;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Annoying2Main();
	}
	public Annoying2Main() {
		super();
		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());
		panels = new ArrayList<Annoying2Panel>();

		Annoying2Panel j = new Annoying2Panel("north", 200);
		pane.add(j, BorderLayout.NORTH);
		panels.add(j);
		j = new Annoying2Panel("east", 300);
		pane.add(j, BorderLayout.EAST);
		panels.add(j);
		j = new Annoying2Panel("south", 400);
		pane.add(j, BorderLayout.SOUTH);
		panels.add(j);
		j = new Annoying2Panel("west", 500);
		pane.add(j, BorderLayout.WEST);
		panels.add(j);

		threadButton = new JButton("Start annoying me!");
		pane.add(threadButton, BorderLayout.CENTER);
		threadButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				doit();
			}
		});
		// Handle window-closing event
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				System.exit(0);
			}
		});

		pack();
		setVisible(true);
	}
	
	private void doit() {
		for (Annoying2Panel p : panels) {
			if (p.isAlive()) {
				p.stop();
			} else {
				p.start();
			}
		}
	}
}
