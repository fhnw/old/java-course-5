package ch.kri.fhnw.Threads;

import java.awt.Color;
import java.awt.Container;

import javax.swing.JFrame;

public class AnnoyingThread extends Thread {
	private JFrame parent;
	private boolean stop;

	public AnnoyingThread(JFrame parent, String name) {
		super(name);
		this.parent = parent;
		this.stop = false;
	}

	public void pleaseStop() {
		stop = true;
	}

	public void run() {
		Container parentPane = parent.getContentPane();
		while (!stop) {
			if (!parentPane.getBackground().equals(Color.WHITE)) {
				parentPane.setBackground(Color.WHITE);
			} else {
				parentPane.setBackground(Color.BLACK);
			}
			parent.repaint();
			try {
				sleep(250);
			} catch (InterruptedException e) {
			}
		}
	}
}
