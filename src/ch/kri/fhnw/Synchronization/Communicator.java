package ch.kri.fhnw.Synchronization;

class Communicator {
	private volatile int counter;

	Communicator() {
		counter = 0;
	}

	int nextNumber1() {
		return ++counter;
	}

	int nextNumber2() {
		int startValue = counter;
		expensiveCalculations(startValue);
		// We finally have our result (really just ++)
		counter = ++startValue;
		return startValue;
	}

	synchronized int nextNumber3() {
		int startValue = counter;
		expensiveCalculations(startValue);
		// We finally have our result (really just ++)
		counter = ++startValue;
		return startValue;
	}

	int nextNumber4() {
		int startValue = counter;
		expensiveCalculations(startValue);
		// We finally have our result (really just ++)
		counter = ++startValue;
		return startValue;
	}

	private int expensiveCalculations(int in) {
		// Lots of time-consuming calculations, as though we
		// actually had something to do.
		double x = 1.0, y = 0.0, z = 0.0;
		for (int i = 0; i < 10000; ++i) {
			x = Math.sin((x * i % 35) * 1.13);
			y = Math.log(x + 10.0);
			z = Math.sqrt(x + y);
		}
		return (in - (int) z);
	}
}