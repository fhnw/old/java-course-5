package ch.kri.fhnw.Synchronization;

/**
 * This program demonstrates the need for synchronization. It creates several threads that
 * communicate via a common object. Theoretically, the threads ought to count from 1to 100. Try the
 * results using the various methods.
 * 
 * @author Brad Richards
 * 
 */
public class SynchTest extends Thread {
	private String name;
	private Communicator comm;

	public static void main(String[] args) {
		Thread[] threads = new Thread[10];
		Communicator comm = new Communicator();
		for (int i = 0; i < 10; ++i) {
			threads[i] = new SynchTest("Thread-" + i, comm);
		}
		for (Thread t : threads) {
			t.start();
		}
	}

	public SynchTest(String name, Communicator comm) {
		this.name = name;
		this.comm = comm;
	}

	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Count " + comm.nextNumber3() + " (" + name + ")");
		}
	}
}

