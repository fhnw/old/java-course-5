package ch.kri.fhnw.WebServer1;

import java.io.*;
import java.net.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WebServer1 extends JFrame {
	Thread serverThread; // the thread we will use for the server

	// GUI stuff
	private JLabel lblPort;
	private JTextField txtPort;
	private JButton btnGo;
	private JScrollPane paneContent;
	private JTextArea txtLog;

	public static void main(String args[]) {
		new WebServer1();
	}

	/** Creates new form WebServer1 */
	public WebServer1() {
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		
		Box topBox = Box.createHorizontalBox();
		pane.add(topBox, BorderLayout.NORTH);

		lblPort = new JLabel("Port");
		topBox.add(lblPort);
		txtPort = new JTextField("80");
		txtPort.setPreferredSize(new Dimension(150, 21));
		topBox.add(txtPort);
		btnGo = new JButton("Go");
		topBox.add(btnGo);
		btnGo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				btnGoMouseClicked(evt);
			}
		});
		
		JScrollPane paneContent = new JScrollPane();
		paneContent.setPreferredSize(new Dimension(300, 300));
		txtLog = new JTextArea();
		txtLog.setFont(new Font("Courier New", 0, 12));
		paneContent.setViewportView(txtLog);
		pane.add(paneContent, BorderLayout.CENTER);
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				exitForm(evt);
			}
		});

		pack();
		setVisible(true);
	}

	private void exitForm(WindowEvent evt) {
		System.exit(0);
	}

	private void btnGoMouseClicked(MouseEvent evt) {
		// Create the server thread and specify its contents
		serverThread = new Thread() {
			public void run() {
				try {
					int port = Integer.parseInt(txtPort.getText());

					ServerSocket listener = new ServerSocket(port, 10, null);
					txtLog.setText("Listening on port " + txtPort.getText() + "\n");

					while (true) {
						// The "accept" method waits for a request, then creates a socket
						// connected to the requesting client
						Socket client = listener.accept();

						txtLog.setText(txtLog.getText() + "Request from client "
								+ client.getInetAddress().toString() + " for server (me) "
								+ client.getLocalAddress().toString() + "\n");

						// Create input and output streams to talk to the client
						BufferedReader inClient = new BufferedReader(new InputStreamReader(client
								.getInputStream()));
						PrintWriter outClient = new PrintWriter(client.getOutputStream());

						// Send our reply using HTTP 1.0
						outClient.print("HTTP/1.0 200 \n"); // Version and status
						outClient.print("Content-Type: text/plain\n");
						outClient.print("\n");

						// Read request from client and send it straight back
						String inString;
						while ((inString = inClient.readLine()) != null) {
							if (inString.length() == 0) break; // an empty line is the end of the request
							outClient.print(inString + "\n");
						}

						outClient.close();
						inClient.close();
						client.close();
					}
				} catch (Exception e) {
					System.err.println(e);
				}
			}
		};

		serverThread.start();
	}
}
