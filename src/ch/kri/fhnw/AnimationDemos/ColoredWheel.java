package ch.kri.fhnw.AnimationDemos;

import java.awt.Color;
import java.awt.Graphics;

public class ColoredWheel extends Thread {
	private boolean stopThread;
	private int xSize, ySize; // size of field
	private int radius; // size of wheel itself
	private int xPos, yPos; // location of wheel center
	private int wheelAngle; // angle of wheel
	private int posAngle; // angle of wheel position
	
	public ColoredWheel(int xSize, int ySize) {
		this.stopThread = false;
		this.xSize = Math.min(xSize, ySize);
		this.ySize = Math.min(xSize, ySize);
		this.radius = (Math.min(xSize, ySize)/4);
		this.xPos = radius;
		this.yPos = radius;
	}
	
	public void stopThread() {
		stopThread = true;
	}
	
	@Override
	public void run() {
		while (!stopThread) {
			wheelAngle += 2;
			if (wheelAngle > 360) wheelAngle -= 360;
			posAngle -= 1; 
			if (posAngle < 0) posAngle += 360;
			
			// Determine new wheel location
			double angle = posAngle * 2 * Math.PI / 360;
			xPos = (int)((float) radius * (1.0 +  Math.sin(angle)));
			yPos = (int)((float) radius * (1.0 +  Math.cos(angle)));
			
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}

		}
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawOval(0, 0, xSize, ySize);
		g.setColor(Color.GREEN);
		g.fillArc(xPos, yPos, radius*2, radius*2, wheelAngle, 120);
		g.drawArc(xPos, yPos, radius*2, radius*2, wheelAngle, 120);
		g.setColor(Color.BLUE);
		g.fillArc(xPos, yPos, radius*2, radius*2, wheelAngle, -120);
		g.drawArc(xPos, yPos, radius*2, radius*2, wheelAngle, -120);
		g.setColor(Color.RED);
		g.fillArc(xPos, yPos, radius*2, radius*2, wheelAngle+120, 120);
		g.drawArc(xPos, yPos, radius*2, radius*2, wheelAngle+120, 120);
	}
}
