package ch.kri.fhnw.AnimationDemos;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JApplet;

public class Animation2 extends JApplet implements Runnable {
	private int frameRate;
	private ColoredWheel wheel;
	private Thread displayThread;
	private boolean started;
	private boolean stopThread;
	
	@Override
	public void init() {
		frameRate = 30;
		wheel = new ColoredWheel(400,400);
		displayThread = new Thread(this);
		setBackground(Color.WHITE);
		stopThread = false;
		started = false;
		wheel.start();
		displayThread.start();
	}

	@Override
	public void destroy() {
		wheel.stopThread();
		wheel.interrupt();
		stopThread = true;
	}
	
	@Override
	public void start() {
		started = true;
	}
	
	@Override
	public void stop() {
		started = false;
	}
	
	@Override
	public void run() {
		while (!stopThread) {
			if (started) this.repaint();
			
			try {
				Thread.sleep(1000 / frameRate);
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0,0,400,400);
		wheel.paint(g);
	}
}
