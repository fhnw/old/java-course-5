package ch.kri.fhnw.AnimationDemos;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JApplet;

public class Animation3 extends JApplet implements Runnable {
	private int frameRate;
	private ColoredWheel wheel;
	private Thread displayThread;
	private boolean started;
	private boolean stopThread;
	private BufferedImage buff; // Buffer for off-screen image
	private Graphics gBuff; // Graphics object for buffer
	
	@Override
	public void init() {
		// Create the buffer and graphics object
		buff = new BufferedImage(401,401, BufferedImage.TYPE_INT_RGB);
		gBuff = buff.createGraphics();

		frameRate = 30;
		wheel = new ColoredWheel(400,400);
		displayThread = new Thread(this);
		setBackground(Color.WHITE);
		stopThread = false;
		started = false;
		wheel.start();
		displayThread.start();
	}
	
	@Override
	public void destroy() {
		wheel.stopThread();
		wheel.interrupt();
		stopThread = true;
	}
	
	@Override
	public void start() {
		started = true;
	}
	
	@Override
	public void stop() {
		started = false;
	}
	
	@Override
	public void run() {
		while (!stopThread) {
			if (started) this.repaint();
			
			try {
				Thread.sleep(1000 / frameRate);
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		gBuff.setColor(Color.WHITE);
		gBuff.fillRect(0,0,401,401);
		wheel.paint(gBuff);
		g.drawImage(buff, 0, 0, buff.getWidth(), buff.getHeight(), this);
	}
}
