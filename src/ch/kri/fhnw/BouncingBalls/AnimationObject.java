package ch.kri.fhnw.BouncingBalls;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

abstract class AnimationObject implements Runnable {
  protected String name;
  protected Color color;
  protected double xPosition, yPosition; // maintained internally as double, to prevent rounding problems
  protected Rectangle bounds;

  protected Thread objectThread;
  protected boolean running;
  
  protected int updatePeriod = 10; // update period in milliseconds

  // Initial information about the object: name and color, position, speed, and the area
  // in which the object moves. Derived classes may have additional information, but this
  // is the common core.
  AnimationObject(String name, Color color, int xPos, int yPos,  Rectangle bounds) {
      this.name = name;
      this.color = color;
      this.xPosition = xPos;
      this.yPosition = yPos;
      this.bounds = bounds;
  }
  
  public void start() {
      running = true;
      
      // initialize the thread for this object
      objectThread = new Thread(this);
      objectThread.setName(this.name);
      objectThread.start();
  }
  
  public void stop() {
      running = false;
  }
  
  // The run method is responsible for moving the object around. How this works
  // may vary according to the object - hence, the method is abstract.
  abstract public void run();
  abstract public void paint(Graphics g);
  
  // return position as an integer - suitable as pixel-position
  public int getXposition() {
  	return (int) xPosition;
  }
  public int getYposition() {
  	return (int) yPosition;
  }
}