package ch.kri.fhnw.BouncingBalls;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JApplet;

public class BouncingBall1 extends JApplet implements Runnable {
	private Thread thread;
	private volatile boolean running; // a variable indicating that the thread should stop.
	// "volatile" means that this variable is accessed from
	// multiple threads, and may not be cached by the JVM
	private ArrayList<AnimationObject> objects;
	private Rectangle field;
	private BufferedImage buff; // Buffer for off-screen image
	private Graphics gBuff; // Graphics object for buffer

	public void init() {
		field = this.getBounds();
		buff = new BufferedImage(field.width, field.height, BufferedImage.TYPE_INT_RGB);
		gBuff = buff.createGraphics();
		objects = new ArrayList<AnimationObject>();
		objects.add(new AnimatedGif("dino.gif", Color.WHITE, (int) field.getMaxX(), (int) field.getMaxY()/2, field));
		objects.add(new AnimatedBall("red ball", Color.RED, 10, 10, 150, 1.2, field, 5));
		objects.add(new GravityBall("blue ball", Color.BLUE, 100, 100, 40, -30, field, 12));
	}

	public void destroy() {
		for (AnimationObject o : objects) {
			o.stop();
		}
	}

	public void start() {
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public void stop() {
		running = false;
	}

	public void paint(Graphics g) {
		gBuff.setColor(Color.WHITE);
		gBuff.fillRect(field.x, field.y, field.width, field.height);
		for (AnimationObject o : objects) {
			o.paint(gBuff);
		}
		g.drawImage(buff, 0, 0, buff.getWidth(), buff.getHeight(), this);
	}

	public void run() {
		while (running) { // continue until we are told to stop
			repaint();

			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
			}
		}
		thread = null; // dereference the thread - not really necessary, but a nicety
	}
}
