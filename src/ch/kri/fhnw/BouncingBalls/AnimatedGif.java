package ch.kri.fhnw.BouncingBalls;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;

public class AnimatedGif extends AnimationObject {
	private Image image;

	AnimatedGif(String name, Color color, int pos, int pos2, Rectangle bounds) {
		super(name, color, pos, pos2, bounds);
		java.net.URL imageURL = AnimatedGif.class.getResource(name);
			image = Toolkit.getDefaultToolkit().createImage(imageURL);
		
		// after initialization, start the thread for this object
		super.start();
	}

	@Override
	public void paint(Graphics g) {
		if (image != null) {
			g.drawImage(image, (int) xPosition, (int) yPosition, color, null);
		} else {
		}
	}

	@Override
	public void run() {
		while (running) { // continue until we are told to stop
			if ((xPosition + image.getWidth(null)) > 0) {
				xPosition -= 0.8;
			} else {
				xPosition = bounds.getMaxX();
			}
			try {
				Thread.sleep(updatePeriod);
			} catch (InterruptedException e) {
			}
		}
	}
}
