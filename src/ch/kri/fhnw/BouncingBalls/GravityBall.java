package ch.kri.fhnw.BouncingBalls;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class GravityBall  extends AnimationObject implements Runnable {
	private double radius;
  protected double xVelocity, yVelocity;

	public GravityBall(String name, Color color, int xPos, int yPos, double xVel, double yVel,
			Rectangle bounds, int radius) {
		// perform all initialization for this object
		super(name, color, xPos, yPos, bounds); // initialize the super-class
		this.xVelocity = xVel;
		this.yVelocity = yVel;
		this.radius = radius;

		// after initialization, start the thread for this object
		super.start();
	}

	public void paint(Graphics g) {
		int xRadius = (int) radius;
		int yRadius = (int) radius;
		int xLeft = (int) (xPosition - radius);
		int yTop = (int) (yPosition - radius);
		g.setColor(color);
		boolean xAdapted = false;

		if ((xPosition - radius) < bounds.getMinX()) { // squish on West wall
			xRadius = (int) (xPosition - bounds.getMinX());
			xLeft = (int) bounds.getMinX();
			xAdapted = true;
		} else if ((xPosition + radius) > bounds.getMaxX()) { // squish on East wall
			xRadius = (int) (bounds.getMaxX() - xPosition);
			xLeft = (int) (bounds.getMaxX() - 2 * xRadius);
			xAdapted = true;
		}
		yRadius = (int) (1.5 * radius - xRadius / 2);
		yTop = (int) yPosition - yRadius;

		if ((yPosition - radius) < bounds.getMinY()) { // squish on West wall
			yRadius = (int) (yPosition - bounds.getMinY());
			yTop = (int) bounds.getMinY();
		} else if ((yPosition + radius) > bounds.getMaxY()) { // squish on East wall
			yRadius = (int) (bounds.getMaxY() - yPosition);
			yTop = (int) (bounds.getMaxY() - 2 * yRadius);
		}
		if (!			xAdapted) {
		xRadius = (int) (1.5 * radius - yRadius / 2);
		xLeft = (int) xPosition - xRadius;
		}

		g.fillOval(xLeft, yTop, 2 * xRadius, 2 * yRadius);
		g.drawString(name, (int) (xPosition + radius + 5), (int) (yPosition + radius));
	}

	// The run method calculates the ball's movement. In this version, we do
	// this "simply": the x and y vectors never interact, which means that we
	// can just keep track of them separately, using a velocity vector (speed + direction) along
	// with sine and cosine functions.

	public void run() {
		while (running) { // continue until we are told to stop
			// Gravity
			yVelocity = yVelocity + 500 * updatePeriod / 1000.0;
			
			double xSpeed = xVelocity * updatePeriod / 1000.0;
			double ySpeed = yVelocity * updatePeriod / 1000.0;
			double newXpos = xPosition + xSpeed;
			double newYpos = yPosition + ySpeed;

			// Check for walls
			if ((newXpos - radius/2) < bounds.getMinX()) { // bounce off West wall
				newXpos = 2 * bounds.getMinX() + radius - xSpeed - xPosition;
				xVelocity = -xVelocity;
			} else if ((newXpos + radius/2) > bounds.getMaxX()) { // bounce off East wall
				newXpos = 2 * bounds.getMaxX() - radius - xSpeed - xPosition;
				xVelocity = -xVelocity;
			}
			xPosition = newXpos;

			if ((newYpos - radius/2) < bounds.getMinY()) { // bounce off North wall
				newYpos = 2 * bounds.getMinY() + radius - ySpeed - yPosition;
				yVelocity = -yVelocity;
			} else if ((newYpos + radius/2) > bounds.getMaxY()) { // bounce off South wall
				newYpos = 2 * bounds.getMaxY() - radius - ySpeed - yPosition;
				yVelocity = -yVelocity;
			}
			yPosition = newYpos;

			try {
				Thread.sleep(updatePeriod);
			} catch (InterruptedException e) {
			}
		}
		objectThread = null; // de-reference the thread - not necessary, but nice
	}
}