package ch.kri.fhnw.BouncingBalls;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

class AnimatedBall extends AnimationObject implements Runnable {
	private double radius;
  protected double speed, direction; // speed in pixels/second, direction in radians

	public AnimatedBall(String name, Color color, int xPos, int yPos, double speed, double direction,
			Rectangle bounds, int radius) {
		// perform all initialization for this object
		super(name, color, xPos, yPos, bounds); // initialize the super-class
		this.speed = speed;
		this.direction = direction;
		this.radius = radius;

		// after initialization, start the thread for this object
		super.start();
	}

	public void paint(Graphics g) {
		g.setColor(color);
		g.fillOval((int) (xPosition- radius), (int) (yPosition-radius), (int) (2 * radius), (int) (2 * radius));
		g.drawString(name, (int) (xPosition + 2 * radius + 5), (int) (yPosition + 2 * radius));
	}

	// The run method calculates the ball's movement. In this version, we do
	// this "correctly", using a velocity vector (speed + direction) along
	// with sine and cosine functions.
	public void run() {
		while (running) { // continue until we are told to stop
			double xSpeed = Math.sin(direction) * speed * updatePeriod / 1000.0;
			double ySpeed = Math.cos(direction) * speed * updatePeriod / 1000.0;
			double newXpos = xPosition + xSpeed;
			double newYpos = yPosition + ySpeed;

			// Check for walls
			if ((newXpos - radius) < bounds.getMinX()) { // bounce off West wall
				newXpos = 2 * bounds.getMinX() + 2 * radius - xSpeed - xPosition;
				direction = 2 * Math.PI - direction;
			} else if ((newXpos + radius) > bounds.getMaxX()) { // bounce off East wall
				newXpos = 2 * bounds.getMaxX() - 2 * radius - xSpeed - xPosition;
				direction = 2 * Math.PI - direction;
			}
			xPosition = newXpos;

			if ((newYpos - radius) < bounds.getMinY()) { // bounce off North wall
				newYpos = 2 * bounds.getMinY() + 2 * radius - ySpeed - yPosition;
				direction = Math.PI - direction;
			} else if ((newYpos + radius) > bounds.getMaxY()) { // bounce off South wall
				newYpos = 2 * bounds.getMaxY() - 2 * radius - ySpeed - yPosition;
				direction = Math.PI - direction;
			}
			yPosition = newYpos;

			// Normalize direction - just for niceness...
			if (direction < 0) direction = direction + 2 * Math.PI;
			if (direction > Math.PI * 2) direction = direction - 2 * Math.PI;
			try {
				Thread.sleep(updatePeriod);
			} catch (InterruptedException e) {
			}
		}
		objectThread = null; // de-reference the thread - not necessary, but nice
	}
}
