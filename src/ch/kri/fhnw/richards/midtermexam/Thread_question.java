package ch.kri.fhnw.richards.midtermexam;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Thread_question extends JFrame {
	private ArrayList<TestThread> threads;
	private int maxThreads;
	private JTextField txtNumThreads;
	private JTextArea txtResults;

	public static void main(String[] args) {
		new Thread_question();
	}

	private Thread_question() {
		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());
		Box topControls = Box.createHorizontalBox();
		topControls.add(new JLabel("Number of threads"));
		txtNumThreads = new JTextField("1000");
		topControls.add(txtNumThreads);
		topControls.add(Box.createHorizontalStrut(20));
		JButton btnGo = new JButton("Go");
		btnGo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				startTest();
			}
		});
		topControls.add(btnGo);
		pane.add(topControls, BorderLayout.NORTH);

		txtResults = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(txtResults);
		scrollPane.setPreferredSize(new Dimension(500, 500));
		pane.add(scrollPane, BorderLayout.CENTER);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				System.exit(0);
			}
		});

		threads = new ArrayList<TestThread>();

		this.pack();
		this.setVisible(true);
	}

	private void startTest() {
		// Create 10 threads
		for (int i = 0; i < 10; i++) {
			TestThread tmpThread = new TestThread(Integer.toString(9 - i), txtResults);
			threads.add(tmpThread);
			tmpThread.start();
		}
	}
}

/**
 * Threads of this class do almost nothing but sleep. We use them to measure the overhead of
 * creating threads.
 */
class TestThread extends Thread {
	private volatile static SharedObject sharedObject;
	private JTextArea txtResults;
	private static Random random = new Random();

	private int times;

	TestThread(String name, JTextArea txtResults) {
		super(name);
		times = 0;
		sharedObject = new SharedObject();
		this.txtResults = txtResults;
	}

	@Override
	public void run() {
		while (times < 3) {
			try {
				sharedObject.updateSharedString(this.getName(), txtResults);
				times++;
				Thread.sleep((int) (TestThread.random.nextFloat() * 90 + 10));
			} catch (InterruptedException e) {
			}
		}
	}

}

class SharedObject {
	private String sharedString = "";
	
	synchronized void updateSharedString(String name, JTextArea txtResults) {
		sharedString += name;
		txtResults.append(name + ": " + sharedString + "\n");
	}
}