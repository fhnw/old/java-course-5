package ch.kri.fhnw.Babble;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Babble1 extends JFrame {
	private Hashtable<String, Integer> analysis = new Hashtable<String, Integer>();
	private int numberOfKeys = 0;

	// GUI controls
	private JLabel jLabel1;
	private JTextField txtFileIn;
	private JButton btnBabble;
	private JTextArea txtOut;
	private Box controls;
	private JComboBox cmbWidth;
	private final JFileChooser fileChooser = new JFileChooser();

	public static void main(String args[]) {
		new Babble1().setVisible(true);
	}

	public Babble1() {
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());

		controls = Box.createHorizontalBox();
		pane.add(controls, BorderLayout.NORTH);

		jLabel1 = new JLabel();
		jLabel1.setText("Input Datei");
		controls.add(jLabel1);

		txtFileIn = new JTextField();
		controls.add(txtFileIn);
		
		JButton btnBrowse = new JButton("Browse");
		btnBrowse.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				int returnVal = fileChooser.showOpenDialog(null);
				if(returnVal == JFileChooser.APPROVE_OPTION) {
					txtFileIn.setText(fileChooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		controls.add(btnBrowse);
		

		controls.add(Box.createHorizontalStrut(30));

		String[] widths = { "1", "2", "3", "4", "5", "6", "7" };
		cmbWidth = new JComboBox(widths);
		cmbWidth.setSelectedIndex(0);
		controls.add(cmbWidth);

		controls.add(Box.createHorizontalStrut(30));

		btnBabble = new JButton("Babble");
		btnBabble.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				btnBabbleMouseClicked(evt);
			}
		});
		controls.add(btnBabble);

		txtOut = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(txtOut);
		Dimension txtSize = new Dimension(500, 500);
		scrollPane.setPreferredSize(txtSize);
		pane.add(scrollPane, BorderLayout.CENTER);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				System.exit(0);
			}
		});

		pack();
		setVisible(true);
	}

	private void btnBabbleMouseClicked(MouseEvent evt) {
		String errorMessage;
		String fileNameIn = txtFileIn.getText();
		File fileIn = new File(fileNameIn);

		// Check the input file
		if (!fileIn.exists()) { // does not exist
			errorMessage = fileNameIn + "' does not exist!";
			JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (!fileIn.isFile()) { // is a directory
			errorMessage = fileNameIn + "' is not a file!";
			JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (!fileIn.canRead()) { // no read privileges
			errorMessage = fileNameIn + "' cannot be read!";
			JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		// everything seems to be ok
		BufferedReader streamIn = null;
		try {
			// Create the input stream
			streamIn = new BufferedReader(new FileReader(fileIn));

			// In a loop - read a character, update the analysis - until EOF
			String key;
			int value;
			Integer valueObject;
			int width = cmbWidth.getSelectedIndex();
			char[] window = new char[width + 1];
			for (int i = 0; i < width; i++) {
				window[i] = (char) streamIn.read();
			}
			int intChar = streamIn.read();
			while (intChar != -1) {
				window[width] = (char) intChar;
				key = new String(window);
				if (analysis.containsKey(key)) {
					valueObject = analysis.get(key);
					value = valueObject.intValue();
					analysis.put(key, new Integer(value + 1));
				} else { // new
					analysis.put(key, new Integer(1));
				}
				numberOfKeys++;

				// shift window
				for (int i = 0; i < width; i++) {
					window[i] = window[i + 1];
				}
				intChar = streamIn.read();
			}
		} catch (Exception e) {
		} finally { // Always close the stream
			if (streamIn != null) try {
				streamIn.close();
			} catch (IOException e2) {
			}
		}

		// the analysis is finished
		txtOut.setText(Integer.toString(numberOfKeys) + " patterns seen, giving " + analysis.size()
				+ " different keys\n");

		for (Iterator<String> i = analysis.keySet().iterator(); i.hasNext();) {
			String key = i.next();
			txtOut.append(key + " - " + analysis.get(key) + "\n");
		}
	}

}
