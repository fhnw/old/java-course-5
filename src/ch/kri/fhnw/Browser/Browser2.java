package ch.kri.fhnw.Browser;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.*;
import javax.swing.*;

public class Browser2 extends JFrame {
	private JLabel lblUrl;
	private JTextField txtUrl;
	private JButton btnGo;
	private JScrollPane paneInhalt;
	private JTextArea txtInhalt;

	public static void main(String[] args) {
		new Browser2();
	}

	/** Creates new form Browser1 */
	public Browser2() {
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		
		Box topBox = Box.createHorizontalBox();
		pane.add(topBox, BorderLayout.NORTH);
		
		lblUrl = new JLabel("URL");
		topBox.add(lblUrl);

		txtUrl = new JTextField();
		txtUrl.setText("http://www.kri.ch/");
		txtUrl.setPreferredSize(new Dimension(400, 21));
		topBox.add(txtUrl);

		btnGo = new JButton("Go");
		topBox.add(btnGo);
		btnGo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				btnGoMouseClicked(evt);
			}
		});

		paneInhalt = new JScrollPane();
		paneInhalt.setPreferredSize(new Dimension(600, 300));
		txtInhalt = new JTextArea();
		txtInhalt.setFont(new Font("Courier New", 0, 12));
		paneInhalt.setViewportView(txtInhalt);
		pane.add(paneInhalt, BorderLayout.CENTER);

		// Handle window-closing event
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				exitForm(evt);
			}
		});

		pack();
		setVisible(true);
	}

	/** Exit the Application */
	private void exitForm(WindowEvent evt) {
		System.exit(0);
	}

	private void btnGoMouseClicked(MouseEvent evt) {
		URL url;
		URLConnection in = null;
		BufferedReader inReader = null;
		String lineIn;
		String urlInhalt;
		String headers;

		// It is possible that we will fail to load the URL...
		try {
			// Use text from txtUrl as URL; create a new URL-Object
			url = new URL(txtUrl.getText());

			// Try to read the content of this URL using HttpURLConnection.
			// "openConnection" creates an URLConnection, which we then cast
			// to HttpURLConnection. This gives us additional methods that
			// are useful for HTTP
			in = url.openConnection(); // may throw Exception
			
			headers = "Content encoding: " + in.getContentEncoding() + "\n";
			headers += "Content type: " + in.getContentType() + "\n";
			headers += "Last modified: " + in.getLastModified() + "\n\n";

			// But we want characters! InputStreamReader transforms a byte-sequence
			// into a char-sequence, which we then buffer. This lets us use the nice
			// method "ReadLine".
			inReader = new BufferedReader(new InputStreamReader(in.getInputStream()));
			urlInhalt = new String();
			while ( (lineIn = inReader.readLine()) != null) {
				urlInhalt += lineIn + "\n";
			}

			// Show the result in "txtInhalt"
			txtInhalt.setText(headers + urlInhalt);
		}

		// If an error occurred, show the error message in txtInhalt
		catch (Exception err) {
			txtInhalt.setText("ERROR: " + err.toString());
		}
	}
}
