package ch.kri.fhnw.Browser;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.*;
import javax.swing.*;

public class Browser1 extends JFrame {
	private JLabel lblUrl;
	private JTextField txtUrl;
	private JButton btnGo;
	private JScrollPane paneInhalt;
	private JTextArea txtInhalt;

	public static void main(String[] args) {
		new Browser1();
	}

	/** Creates new form Browser1 */
	public Browser1() {
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		
		Box topBox = Box.createHorizontalBox();
		pane.add(topBox, BorderLayout.NORTH);
		
		lblUrl = new JLabel("URL");
		topBox.add(lblUrl);

		txtUrl = new JTextField();
		txtUrl.setText("http://www.google.com/");
		txtUrl.setPreferredSize(new Dimension(400, 21));
		topBox.add(txtUrl);

		btnGo = new JButton("Go");
		topBox.add(btnGo);
		btnGo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				btnGoMouseClicked(evt);
			}
		});

		paneInhalt = new JScrollPane();
		paneInhalt.setPreferredSize(new Dimension(600, 300));
		txtInhalt = new JTextArea();
		txtInhalt.setFont(new Font("Courier New", 0, 12));
		paneInhalt.setViewportView(txtInhalt);
		pane.add(paneInhalt, BorderLayout.CENTER);

		// Handle window-closing event
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				exitForm(evt);
			}
		});

		pack();
		setVisible(true);
	}

	/** Exit the Application */
	private void exitForm(WindowEvent evt) {
		System.exit(0);
	}

	private void btnGoMouseClicked(MouseEvent evt) {
		URL url;
		InputStream in = null;
		InputStreamReader inStr = null;
		char[] buffer = new char[1024];
		int bufferLen;
		String urlInhalt;

		// It is possible that we will fail to load the URL...
		try {
			// Use text from txtUrl as URL; create a new URL-Object
			url = new URL(txtUrl.getText());

			// Try to read the content of this URL. "openStream" creates an
			// instance of the class "InputStream" - this class returns a
			// sequence of bytes
			in = url.openStream(); // may throw Exception

			// But we want characters! InputStreamReader transforms a byte-sequence
			// into a char-sequence. The InputStream is the parameter and the return
			// value is the Reader. When we call "read", the buffer is filled with
			// characters, and the method returns the number of characters read.
			inStr = new InputStreamReader(in);
			urlInhalt = new String();
			while ((bufferLen = inStr.read(buffer)) != -1) {
				urlInhalt = urlInhalt + new String(buffer, 0, bufferLen);
			}

			// Show the result in "txtInhalt"
			txtInhalt.setText(urlInhalt);
		}

		// If an error occurred, show the error message in txtInhalt
		catch (Exception err) {
			txtInhalt.setText("ERROR: " + err.toString());
		}
	}
}
