package ch.kri.richards.babble;

import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.util.*;

public class BabbleReal extends javax.swing.JFrame {
	private static final long serialVersionUID = 179430138313101530L;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JTextField txtFileIn;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JTextField txtFileOut;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JTextField txtLength;
	private javax.swing.JButton btnBabble;
	private javax.swing.JButton btnClose;

	Random rand = new Random();

	public BabbleReal() {
		initComponents();
	}

	private void initComponents() {// GEN-BEGIN:initComponents
		jLabel1 = new javax.swing.JLabel();
		txtFileIn = new javax.swing.JTextField();
		jLabel2 = new javax.swing.JLabel();
		txtFileOut = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		txtLength = new javax.swing.JTextField();
		btnBabble = new javax.swing.JButton();
		btnClose = new javax.swing.JButton();

		getContentPane().setLayout(new java.awt.GridLayout(4, 2));

		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				exitForm(evt);
			}
		});

		jLabel1.setText("Input Datei");
		getContentPane().add(jLabel1);

		getContentPane().add(txtFileIn);

		jLabel2.setText("Output Datei");
		getContentPane().add(jLabel2);

		getContentPane().add(txtFileOut);

		jLabel3.setText("Length");
		getContentPane().add(jLabel3);

		txtLength.setText("5");
		getContentPane().add(txtLength);

		btnBabble.setText("Babble");
		btnBabble.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnBabbleMouseClicked(evt);
			}
		});

		getContentPane().add(btnBabble);

		btnClose.setText("Schliessen");
		btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnCloseMouseClicked(evt);
			}
		});

		getContentPane().add(btnClose);

		pack();
	}

	private void btnBabbleMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_btnBabbleMouseClicked
		ShowDialog errDialog;
		Hashtable<String, Hashtable<String, Integer>> analysis = new Hashtable<String, Hashtable<String, Integer>>();
		int total = 0;
		String anfang = "";

		// Die Length pr�fen
		int keyLength = 0;
		keyLength = Integer.parseInt(txtLength.getText());
		keyLength = keyLength - 1; // Length of key is one less than window size
		if ((keyLength < 1) | (keyLength > 6)) {
			errDialog = new ShowDialog("Fehler", "Length muss zwischen 2 und 7 sein!");
			return;
		}

		// Die Eingabe-Datei prufen
		BufferedReader streamIn = null;
		String fileNameIn = txtFileIn.getText();
		File fileIn = new File(fileNameIn);

		if (!fileIn.exists()) { // existiert nicht
			errDialog = new ShowDialog("Fehler", "'" + fileNameIn
					+ "' existiert nicht!");
			errDialog.show();
			return;
		}
		if (!fileIn.isFile()) { // ist ein Verzeichnis
			errDialog = new ShowDialog("Fehler", "'" + fileNameIn
					+ "' ist keine Datei!");
			errDialog.show();
			return;
		}
		if (!fileIn.canRead()) { // Keine Lese-Rechte
			errDialog = new ShowDialog("Fehler", "'" + fileNameIn
					+ "' kann nicht gelesen werden!");
			errDialog.show();
			return;
		}

		// Die Ausgabe-Datei prufen
		FileWriter streamOut = null;
		String fileNameOut = txtFileOut.getText();
		File fileOut = new File(fileNameOut);

		if (fileOut.exists()) { // existiert - wir brauchen Schreibberechtigung
			if (!fileOut.canWrite()) {
				errDialog = new ShowDialog("Fehler", "'" + fileNameOut
						+ "' kann nicht gegeschrieben werden!");
				return;
			}
		} else {
			String directoryName = fileOut.getParent();
			if (directoryName == null) { // kein Pfad, also aktuelles Verzeichnis
				directoryName = System.getProperty("user.dir");
			}
			File directory = new File(directoryName);
			if (!directory.exists()) { // existiert nicht
				errDialog = new ShowDialog("Fehler", "Verzeichnis '" + directoryName
						+ "' existiert nicht!");
				return;
			}
			if (!directory.canWrite()) { // keine Schreibberechtigung
				errDialog = new ShowDialog("Fehler", "Verzeichnis '" + directoryName
						+ "' kann nicht geschrieben werden!");
				return;
			}
		}

		// alles scheint in Ordnung zu sein!
		try {
			char[] window = new char[keyLength];

			String key;

			streamIn = new BufferedReader(new FileReader(fileIn));
			analysis.clear();
			total = 0;

			// In einer Schleife: Zeichen lesen, Analyse updaten - bis Fehler (EOF)
			for (int i = 0; i < keyLength; i++) {
				window[i] = (char) streamIn.read();
			}
			anfang = new String(window); // Remember for generation

			int intChar = streamIn.read();
			while (intChar != -1) {
				key = new String(window);
				addToAnalysis(analysis, key, intChar);
				total++;

				// shift window by one character
				for (int i = 0; i < (keyLength - 1); i++) {
					window[i] = window[i + 1];
				}
				window[keyLength - 1] = (char) intChar;
				intChar = streamIn.read();
			}
			key = new String(window);
			addToAnalysis(analysis, key, -1);
			total++;
		} catch (Exception e) {
		} finally { // Ob Fehler oder nicht, wir mussen das Stream schliessen
			if (streamIn != null)
				try {
					streamIn.close();
				} catch (IOException e2) {
				}
		}

		// Ausgabedatei erzeugen
		String textOut = "";
		try {
			String nextChar = anfang;
			String key = anfang;
			while (nextChar != "END") {
				textOut += nextChar;
				nextChar = getNextChar(analysis, key);
				key = key.substring(1) + nextChar;
			}

			streamOut = new FileWriter(fileOut);
			streamOut.write(textOut);
		} catch (Exception e) {
		} finally {
			if (streamOut != null)
				try {
					streamOut.close();
				} catch (IOException e2) {
				}
		}

		// Die Analyse ist fertig
		ShowDialog zusammenfassung = new ShowDialog("Fertig", Integer
				.toString(total)
				+ " Keys gesehen, davon " + analysis.size() + " unterschiedlich");
		zusammenfassung.show();
	}

	private void addToAnalysis(
			Hashtable<String, Hashtable<String, Integer>> analysis, String key,
			int intChar) {
		int value;
		String nextChar;
		Integer valueObject;
		Hashtable<String, Integer> nextChars;

		if (intChar > 0) {
			nextChar = String.valueOf((char) intChar);
		} else {
			nextChar = "END";
		}

		if (analysis.containsKey(key)) {
			// Get the existing hashtable of entries
			nextChars = analysis.get(key);
			if (nextChars.containsKey(nextChar)) {
				// increment the entry
				valueObject = nextChars.get(nextChar);
				value = valueObject.intValue();
				nextChars.put(nextChar, new Integer(value + 1));
			} else {
				// create a new entry
				nextChars.put(nextChar, new Integer(1));
			}
		} else {
			// new hashtable containing this one entry
			nextChars = new Hashtable<String, Integer>();
			nextChars.put(nextChar, new Integer(1));
			analysis.put(key, nextChars);
		}
	}

	private String getNextChar(
			Hashtable<String, Hashtable<String, Integer>> analysis, String key) {
		Hashtable nextChars;
		Enumeration keys;
		int totalEntries = 0;
		int entryNum;
		Integer valueObject;

		if (analysis.containsKey(key)) {
			nextChars = analysis.get(key);
			keys = nextChars.keys();

			// Count how many possibilities there are altogether
			while (keys.hasMoreElements()) {
				valueObject = (Integer) nextChars.get(keys.nextElement());
				totalEntries += valueObject.intValue();
			}
			entryNum = rand.nextInt(totalEntries);

			keys = nextChars.keys();
			totalEntries = 0;
			String nextChar = "";
			// Get the appropriate key
			while (keys.hasMoreElements()) {
				nextChar = (String) keys.nextElement();
				valueObject = (Integer) nextChars.get(nextChar);
				totalEntries += valueObject.intValue();
				if (totalEntries > entryNum) {
					break;
				}
			}
			return nextChar;
		} else {
			return null;
		}
	}

	private void btnCloseMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_btnCloseMouseClicked
		System.exit(0);
	}// GEN-LAST:event_btnCloseMouseClicked

	/** Exit the Application */
	private void exitForm(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_exitForm
		System.exit(0);
	}// GEN-LAST:event_exitForm

	/**
	 * @param args
	 *          the command line arguments
	 */
	public static void main(String args[]) {
		new BabbleReal().setVisible(true);
	}

	class ShowDialog extends javax.swing.JFrame {

		private static final long serialVersionUID = -6850755002660545717L;
		private JFrame errorDialog;

		public ShowDialog(String title, String message) {
			errorDialog = new JFrame(title);
			Container frameContent = errorDialog.getContentPane();
			frameContent.setLayout(new GridLayout(1, 1));
			JLabel lblError = new JLabel("");
			frameContent.add(lblError);
			lblError.setText(message);
			errorDialog.pack();
			errorDialog.setLocation(100, 100);
			errorDialog.setVisible(true);
		}

		public void show() {
			errorDialog.setLocation(100, 100);
			errorDialog.setVisible(true);
		}

	}
}
